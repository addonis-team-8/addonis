import React, {useState, useEffect} from 'react';
import {Route, Routes} from 'react-router-dom';
import {useAuthState} from 'react-firebase-hooks/auth';
import './App.css';
import Header from './components/Header/Header';
import Home from './views/Home/Home';
import AddonDetailed from './components/AddonDetailed/AddonDetailed';
import Register from './components/Register/Register';
import Login from './components/Login/Login';
import Upload from './components/Upload/Upload';
import UserDetailed from './components/UserDetailed/UserDetailed';
import {auth} from './config/firebase-config';
import {getUserData} from './services/users.service';
import {AppContext} from './providers/AppContext';
import {User} from 'firebase/auth';
import AdminPanel from './components/AdminPanel/AdminPanel';
import UserView from './components/UserView/UserView';
import NotFound from './components/NotFound/NotFound';

const addonObj = {
  name: '',
  description: '',
  creator: '',
  tags: [],
  downloads: 0,
  rating: 0,
  downloadLink: '',
  originLink: '',
  apiLink: '',
  issues: '',
  pulls: '',
  lastCommit: '',
  uploadDate: '',
  approved: false,
  addonLogo: '',
  fileName: '',
  logoName: '',
  ide: '',
  featured: false,
};

const App = (props:any) => {
  const [appState, setAppState] = useState<{user: User | any, userData: any}>({user: null, userData: null});
  const [user] = useAuthState(auth);

  useEffect(() => {
    if (user === null || user === undefined) {
      return;
    }
    getUserData(user?.uid)
        .then((snap) => {
          if (!snap.exists()) {
            return;
          }

          setAppState({user, userData: snap.val()[Object.keys(snap.val())[0]]});
        }).catch((e) => {
          alert(e.message);
        });
  }, [user]);
  return (
    <>
      <AppContext.Provider value={{...appState, setContext: setAppState}}>
        <Header/>
        <Routes>
          <Route path='/' element={<Home/>}/>
          <Route path='/home' element={<Home/>}/>
          <Route path='/addon/:id' element={<AddonDetailed/>}/>
          <Route path='/register' element={<Register/>}/>
          <Route path='/login' element={<Login/>}/>
          <Route path='/upload' element={<Upload handleEditing={function(e: React.ChangeEvent<HTMLInputElement>, prop:string): void {
          } } addon={addonObj}/>}/>
          <Route path='/admin' element={<AdminPanel/>}/>
          <Route path='/user/:username' element={<UserView/>}/>
          <Route path='/profile' element={<UserDetailed/>}/>
          <Route path='*' element={<NotFound/>}/>
        </Routes>
      </AppContext.Provider>
    </>
  );
};

export default App;
