
import {Link} from 'react-router-dom';
interface AddonCardInfo {
  addonLogo: string,
  name: string,
  description: string,
  website: string,
  price: string,
  rating: string,
  tags:Array<string>
}

const AddonCard = (props:AddonCardInfo) => {
  return (
    <Link to={`/addon/${props.name}`} state={{addon: {...props}}}>
      <div className='border border-gray-400 rounded-lg shadow-md dark:bg-zinc-200 w-56 mx-auto h-auto'>
        <div className=''>
          <div className='w-38 h-38 border-2 my-2'>
            <img className='object-cover mx-auto w-35 h-36' alt='Sunset in the mountains' src={props.addonLogo}></img>
          </div>
          <div className='font-bold text-xl mb-2 text-center mt-1 dark:text-gray-900'>{props.name}</div>
          <div className='flex-wrap w-full h-20 w-2/6'>
            <p className='text-gray-700 mx-5 dark:text-gray-800'>
              {props.description}
            </p>
          </div>
          <div className='px-4 pt-4 pb-2'>
            {props.tags?.length > 0 ? props.tags.map((tag) => (
              <span key={tag} className="inline-block bg-white rounded-full px-2 py-1 text-sm font-semibold text-gray-900 mx-1 mb-2 hover:cursor-pointer hover:backdrop-blur-sm shadow-md">{tag}</span>
            )) : <></>}
          </div>
        </div>
      </div>
    </Link>
  );
};
export default AddonCard;
