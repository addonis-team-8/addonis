interface AddonCardInfo {
  addonLogo: string,
  name: string,
  description: string,
  website: string,
  price: string,
  ratings: number,
  tags:Array<string>,
  creator:string,
  downloads:number,
  ide:string,
  downloadLink: string,
  approved: boolean,
  featured: boolean,
}
import {Link} from 'react-router-dom';
import StarRating from './StarRating/StarRating';
import {addDownload, rateAddon} from '../services/addon.service';
import {useEffect, useState, useContext} from 'react';
import {AppContext} from '../providers/AppContext';
const AddonCard2 = (props:AddonCardInfo) => {
  const [addon, setAddon] = useState({...props});
  const [dummyRefresh, setDummyRefresh] = useState(false);
  const [ratingEnabled, setRatingEnabled] = useState(false);
  const [ratingScore, setRatingScore] = useState(0);
  const {userData} = useContext(AppContext);
  const handleDownload = () => {
    addDownload(props.name, props.featured);
    setAddon({
      ...addon,
      ['downloads']: addon.downloads + 1,
    });
    setDummyRefresh(!dummyRefresh);
  };
  useEffect(() => {
    let totalScore = 0;
    if (props?.ratings) {
      Object.values(props.ratings).map((score) => {
        totalScore += score;
      });
      totalScore = totalScore / Object.keys(props.ratings).length;
      setRatingScore(totalScore);
    }
  }, []);
  const handleRating = () => {
    if (props.approved && userData) {
      setRatingEnabled(!ratingEnabled);
    }
  };
  const handleAddonRating = (rating:number) => {
    if (props.approved) {
      rateAddon(props.name, userData.username, rating, props.featured);
    }
  };
  return (
    <div className='w-64 h-80 bg-navbar text-textW shadow mx-auto'>
      <Link to={`/addon/${props.name}`} state={{addon: {...props}}}>
        <div className='h-3/6 my-2'>
          <img src={props.addonLogo} className='object-fit mx-auto h-full my-2'></img>
        </div>
        <div className=' mx-3 text-center mt-8 font-semibold text-xl object-scale-down truncate '>{props.name}</div>
      </Link>
      <div className=' h-10 w-full flex mt-2'>
        <div className=' h-full w-3/6 flex items-center ml-4 font-semibold hover:text-sGreen hover:cursor-pointer '>
          <span className='mr-1 text-gray-500'>by</span><Link to={`/user/${props.creator}`} state={{creator: props.creator}}><span> {props.creator}</span></Link>
        </div>
        {props.approved ? <div className='h-full w-16 flex items-center ml-auto'>
          <a href={props.downloadLink} onClick={handleDownload} rel="noreferrer"><svg className='fill-current w-4 h-full mr-1 mt-1 text-gray-500 hover:text-sGreen hover:cursor-pointer' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'><path d="M13 8V2H7v6H2l8 8 8-8h-5zM0 18h20v2H0v-2z"/></svg></a>
          <span className={props.downloads < 100 ? 'ml-3 text-textW font-semibold' : props.downloads > 999 ? 'ml text-textW font-semibold' : 'ml-2 text-textW font-semibold'}>{addon.downloads}</span>
        </div> : <></> }
      </div>
      <div className='h-10 flex'>
        <div className=' h-full w-3/6 ml-3' onClick={handleRating}>
          <StarRating ratingScore={ratingScore} ratingEnabled={ratingEnabled} handleAddonRating={(rating:number) => handleAddonRating(rating)}/>
        </div>
        <div className='h-full w-2/6 text-right font-semibold text-sm mt-2'>{props.ide}</div>
      </div>
    </div>
  );
};

export default AddonCard2;

{/* <div className='border-2 border-filler w-64 h-80 bg-navbar text-textW shadow mx-auto'>
<Link to={`/addon/${props.name}`} state={{addon: {...props}}}>
  <div className='h-3/6 my-2'>
    <img src={props.addonLogo} className='object-fit mx-auto h-auto w-4/6 my-2'></img>
  </div>
  <div className=' mx-3 text-center mt-8 font-semibold text-xl object-scale-down truncate '>{props.name}</div>
</Link>
<div className=' h-10 w-full flex mt-2'>
  <div className=' h-full w-3/6 flex items-center ml-4 font-semibold hover:text-sGreen hover:cursor-pointer '>
    <span className='mr-1 text-gray-500'>by</span><Link to={`/user/${props.creator}`} state={{creator: props.creator}}><span> {props.creator}</span></Link>
  </div>
  {props.approved ? <div className='h-full w-16 flex items-center ml-auto'>
    <a href={props.downloadLink} onClick={handleDownload} rel="noreferrer"><svg className='fill-current w-4 h-full mr-1 mt-1 text-gray-500 hover:text-sGreen hover:cursor-pointer' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'><path d="M13 8V2H7v6H2l8 8 8-8h-5zM0 18h20v2H0v-2z"/></svg></a>
    <span className={props.downloads < 100 ? 'ml-3 text-textW font-semibold' : props.downloads > 999 ? 'ml text-textW font-semibold' : 'ml-2 text-textW font-semibold'}>{addon.downloads}</span>
  </div> : <></> }
</div>
<div className='h-10 flex'>
  <div className=' h-full w-3/6 ml-3' onClick={handleRating}>
    <StarRating ratingScore={ratingScore} ratingEnabled={ratingEnabled} handleAddonRating={(rating:number) => handleAddonRating(rating)}/>
  </div>
  <div className='h-full w-2/6 text-right font-semibold text-sm mt-2'>{props.ide}</div>
</div>
</div> */}
