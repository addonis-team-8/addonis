/* eslint-disable react/no-children-prop */
/* eslint-disable react/jsx-no-target-blank */
import React, {useEffect, useState, useContext} from 'react';
import {deleteAddonFromStorage, getGitHubData, getRepoMd, approveAddon, featureAddon} from '../../services/addon.service';
// import ReactMarkdown from 'react-markdown';
// import rehypeRaw from 'rehype-raw';
import './AddonDetailed.css';
import Markdown from 'markdown-to-jsx';
import {useNavigate, useLocation} from 'react-router-dom';
import StarRating from '../StarRating/StarRating';
import {addDownload} from '../../services/addon.service';
import Upload from '../Upload/Upload';
import {rateAddon} from '../../services/addon.service';
import {AppContext} from '../../providers/AppContext';
import Swal from 'sweetalert2';
import {createNotification} from '../../services/notification.service';
import Main from '../Main/Main';
import toast from '../Toast/toaster';
interface AddonObject2 {
  name: '',
  description: '',
  creator: '',
  tags: [],
  downloads: 0,
  ratings: 0,
  downloadLink: '',
  originLink: '',
  apiLink: '',
  issues: '',
  pulls: '',
  lastCommit: '',
  uploadDate: '',
  approved: false,
  addonLogo: '',
  fileName: '',
  logoName: '',
  featured: false,
}
interface gitHubData {
  issuesCount: number,
  numberOfPulls: number,
  daysSinceLastCommit: number,
}
const AddonDetailed = (props:any) => {
  const [addonData, setAddonData2] = useState<AddonObject2 | any>();
  const [repoReadme, setRepoReadme] = useState('');
  const [gitHubData, setGitHibData] = useState<gitHubData>();
  const [edinOn, setEditOn] = useState(false);
  const [ratingEnabled, setRatingEnabled] = useState(false);
  const [ratingScore, setRatingScore] = useState(0);
  const [addonLoaded, setAddonLoaded] = useState(false);
  const {userData} = useContext(AppContext);
  const location = useLocation();
  const navigate = useNavigate();
  const {state} = location as any;
  const getData = async (addon:any) => {
    if (addon.apiLink) {
      const repoReadmeInfo = await getRepoMd(addon.apiLink);
      const data = await getGitHubData(addon.apiLink);
      setGitHibData(data);
      setRepoReadme(repoReadmeInfo);
      setAddonLoaded(true);
    }
  };
  useEffect(() => {
    setAddonData2(state.addon);
    getData(state.addon);
  }, []);
  const handleDeleteAddon = async () => {
    await deleteAddonFromStorage(addonData);
    createNotification(state.creator, `${userData.username}|Deleted ${state.name}`, 'An');
    createNotification('admin', `${userData.username}|Deleted ${state.name}`, 'An');
    navigate('/');
  };
  const handleApprove = async () => {
    toast.fire({
      icon: 'success',
      title: 'Addon Approved',
    });
    approveAddon(addonData);
    navigate('/admin');
  };
  const handleEdit = () => {
    setEditOn(!edinOn);
  };
  const handleDownload = () => {
    addDownload(state.addon.name, state.addon.featured);
  };
  const handleFeature = () => {
    featureAddon(state.addon);
    createNotification(state.creator, `${userData.username}|Approved ${state.name}`, 'An');
  };
  const handleEditing = (e: React.ChangeEvent<HTMLInputElement>, prop:string) => {
    setAddonData2({
      ...addonData,
      [prop]: e.target.value,
    });
    const takenProps = ['name', 'originLink', 'description'];
    if (!e.target.value &&
      !takenProps.includes(prop)
    ) {
      let getTags = [...addonData.tags];
      if (getTags.includes(prop)) {
        getTags = getTags.filter((el:string) => el !== prop);
      } else {
        getTags.push(prop);
      }
      setAddonData2({
        ...addonData,
        ['tags']: [...getTags],
      });
    }
  };
  useEffect(() => {
    let totalScore = 0;
    Object.keys(state.addon.ratings).map((key) => {
      totalScore += state.addon.ratings[key];
    });
    totalScore = totalScore / Object.keys(state.addon.ratings).length;
    setRatingScore(totalScore);
  }, []);
  const handleRating = () => {
    if (userData && userData.type !== 'blocked' && state.addon.approved === true) {
      setRatingEnabled(!ratingEnabled);
    };
  };
  const handleAddonRating = (rating:number) => {
    if (userData) {
      rateAddon(state.addon.name, userData.username, rating, state.addon.featured);
    } else {
      Swal.fire({
        title: 'Error',
        text: 'Please login First',
        icon: 'error',
        confirmButtonText: 'Ok',
        background: '#242a2f',
      });
      navigate('/login');
    }
  };
  return (
    <Main title='Addon Details'>
      <div className={edinOn ? 'flex' : 'flex'}>
        <div className={edinOn ? 'border border-filler px-4 w-3/6 mb-10' : 'container mx-auto max-w-7xl mb-10'}>
          <div className='flex mt-5 mb-2 p-4 bg-navbar text-textW'>
            <img src={addonData?.addonLogo} className='object-scale-down h-48 w-48 '></img>
            <div className='mx-4 my-2 w-4/6'>
              {/* // eslint-disable-next-line react/jsx-no-target-blank */}
              <div className='h-1/6 mb-6 mx-2'>
                <a href={addonData?.originLink} target='_blank' className='text-3xl font-semibold text-textW'>{addonData?.name}</a>
              </div>
              <p className='mx-2 text-gray-400 h-2/6 my-4'>{addonData?.description}</p>
              {!edinOn ? <div className='border-navbar mx-2'>
                <h1 className='ml-2 p-0 mt-1 mb-0'>Rating</h1>
                <div onClick={handleRating}>
                  <StarRating ratingScore={ratingScore} ratingEnabled={ratingEnabled} handleAddonRating={(rating:number) => handleAddonRating(rating)}/>
                </div>
              </div> : <></> }
            </div>
            <div className=' my-2 w-1/6'>
              <div className='h-full'>
                <div className='h-3/6 mx-auto w-fit'>
                  <div className='h-3/6'></div>
                  {userData && userData.type === 'admin'&& !edinOn && state.addon.approved ? <button type="button" onClick={handleFeature} className="text-sBlue border-2 border-sBlue bg-black  px-6 py-2 rounded-lg hover:bg-filler hover:text-mainBG font-semibold">{state.addon.featured ? 'Unfeature' : 'Feature'}</button> : <></> }
                </div>
                <div className='w-fit mx-auto my-4 h-1/6'>
                  {userData && (userData.username === state.addon.creator && userData.type !== 'blocked') || userData && userData.type === 'admin' ? <button type="button" onClick={handleEdit} className={userData.type === 'blocked' ? 'text-sRed border-2 border-sRed px-8 py-2 rounded-lg font-semibold ' : 'text-gray-500 border-2 border-textW px-8 py-2 rounded-lg hover:bg-navbar hover:text-textW hover:border-filler font-semibold'}>Edit</button>: <></>}
                </div>
                <div className='w-fit mx-auto h-2/6  border-sGreen'>
                  {!edinOn && state.addon.approved ? <a href={state.addon.downloadLink} onClick={handleDownload} className='mx-2'>
                    <button className='border-2 border-sGreen text-sGreen hover:bg-sGreen hover:text-mainBG font-bold py-2 px-4 rounded inline-flex items-center'>
                      <svg className='fill-current w-4 h-4 mr-2' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'><path d="M13 8V2H7v6H2l8 8 8-8h-5zM0 18h20v2H0v-2z"/></svg>
                      <span>Download</span>
                    </button>
                  </a> : <></> }
                </div>
              </div>
            </div>
          </div>
          <div className='flex'>
            {addonLoaded ? <div id='addon-readme' className='bg-navbar text-center mx-auto'>
              <div id='addon-readme-more' className='p-5 my-2 text-textW border border-navbar bg-fillerD'>
                <Markdown>{repoReadme}</Markdown>
              </div>
            </div> :
            <div id='addon-readme-more' className='p-5 my-1 text-textW border border-navbar bg-fillerD animate-pulse'>
            </div>}
            <div className='w-2/6 bg-navbar px-2'>
              <div className='flex-wrap w-6/6 border border-navbar bg-fillerD h-fit my-2'>
                <div className='flex px-6 py-2'>
                  {userData && userData.type !== 'blocked' && (userData.username === state.addon.creator && !edinOn) || userData && (userData.type === 'admin' && !edinOn) ? <button type="button" onClick={handleDeleteAddon} className="my-4 mx-auto text-sRed border-2 border-sRed px-6 py-2 rounded-lg hover:bg-sRed hover:text-mainBG font-semibold">Delete</button> : <></>}
                  {userData && userData.type === 'admin' && !state.addon.approved && !edinOn ? <button type="button" onClick={handleApprove} className="my-4 mx-auto text-sGreen border-2 border-sGreen px-6 py-2 rounded-lg hover:bg-dGreen hover:text-mainBG font-semibold">Approve</button> : <></>}
                </div>
                <div className='mx-2 bg-zinc-150 py-1'>
                  <h1 className='font-semibold text-textW'>Tags</h1>
                  {addonData?.tags ? addonData?.tags.map((tag:any, key:any) => (
                    <div key={key} id='addon-tag' className='inline-block bg-filler border border-navbar rounded-full px-3 py-1 text-sm font-semibold text-textW cursor-pointer mr-2 mb-2 hover:bg-sGreen hover:text-mainBG'>{tag}</div>
                  )) : <></>}
                </div>
                <div className='mx-2 my-2 py-1'>
                  <h1 className='font-semibold text-textW'>More Stats</h1>
                  <h1 className='text-sm font-semibold text-blue-600 leading-3 tracking-wider'><a href={addonData?.originLink} target='_blank'>GitHub</a></h1>
                  <h1 className='text-sm font-semibold text-blue-600 leading-3 tracking-wider'><a href={addonData?.originLink+'/commits/master'} target='_blank'>Last commit: {gitHubData?.daysSinceLastCommit} days ago</a></h1>
                  <h1 className='text-sm font-semibold text-blue-600 leading-3 tracking-wider'><a href={addonData?.originLink+'/pull'} target='_blank'>{gitHubData?.numberOfPulls} Pull Requests</a></h1>
                  <h1 className='text-sm font-semibold text-blue-600 leading-3 tracking-wider'><a href={addonData?.originLink+'/issues'} target='_blank'>{gitHubData?.issuesCount} issues</a></h1>
                </div>
              </div>
            </div>
          </div>
        </div>
        {edinOn ? <div className='w-3/6 border border-filler px-4'><Upload handleEditing={(e: React.ChangeEvent<HTMLInputElement>, prop:string) => handleEditing(e, prop)} addon={addonData}/></div> : <></>}
      </div>
    </Main>
  );
};

export default AddonDetailed;
