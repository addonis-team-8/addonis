/* eslint-disable no-unused-vars */
// import searchIcon from '../../assets/searchIconT.png';
import {useState, useContext, useEffect} from 'react';
// import PendingAddons from '../PendingAddons/PendingAddons';
import ApprovedAddons from '../ApprovedAddons/ApprovedAddons';
import Users from '../Users/Users';
import {getAddonsByTags, getAddon} from '../../services/addon.service';
import AddonCard2 from '../AddonCard2';
import {getUserByUsername, getUserByPhone, getUserByEmail} from '../../services/users.service';
import PendingAddons from '../PendingAddons/PendingAddons';
import {Link} from 'react-router-dom';
import {AppContext} from '../../providers/AppContext';
import Notification from '../Notification/Notification';
import Main from '../Main/Main';
const AdminPanel = () => {
  const [position, setPosition] = useState('dashboard');
  const [searchAddons, setSearchAddons] = useState<any[]>([]);
  const [prefix, setPrefix] = useState('Tags');
  const [tag, setTag] = useState('');
  const [tags, setTags] = useState(['']);
  const [user, setUser] = useState('');
  const [addonName, setAddonName] = useState('');
  const [userSearch, setUserSearch] = useState<any>();
  const {userData} = useContext(AppContext);
  const [userN, setUserN] = useState<any[]>([]);
  const [addonN, setAddonN] = useState<any[]>([]);
  const [addoUnseenN, setAddonUnseeN] = useState<any[]>([]);
  const [userUnseeN, setUserUnseeN] = useState<any[]>([]);
  const [pendingAddons, setPendingAddons] = useState([]);
  const updateTags = (e:any) => {
    e.target.value = e.target.value.replace(/[^\w\s]/gi, '');
    setTag(e.target.value);
  };
  // eslint-disable-next-line quotes
  const emailRegex = new RegExp("([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])");
  const createTag = (e:any) => {
    e.preventDefault();
    const allTags = [...tags];
    if (allTags[0] === '') {
      allTags[0] = tag.trim();
    } else {
      if (allTags.includes(tag.trim())) {
        setTag('');
      } else {
        allTags.push(tag.trim());
      }
    }
    setTag('');
    setTags(allTags);
  };
  const searchDB = async () => {
    let data:any = null;
    if (prefix === 'Tags') {
      data = await getAddonsByTags(tags);
      setSearchAddons(data);
    } else if (prefix === 'Users') {
      if (/^\d+$/.test(user)) {
        data = await (await getUserByPhone(user)).val();
        if (Object.keys(data).length == 1) {
          Object.keys(data).map((key) => {
            setUserSearch(data[key]);
          });
        }
      } else if (emailRegex.test(user)) {
        data = await (await getUserByEmail(user)).val();
        Object.keys(data).map((key) => {
          setUserSearch(data[key]);
        });
      } else {
        data = await getUserByUsername(user);
        setUserSearch(data.val());
      }
    } else if (prefix === 'Addons') {
      data = await getAddon(addonName);
      setSearchAddons(data);
    }
  };
  const handlePosition = (e:any) => {
    setPosition(e.target.value);
    setUserSearch(null);
    setSearchAddons([]);
  };
  const handlePrefix = (e:any) => {
    setPrefix(e.target.value);
  };
  const handleDeleteTag = (tag:string) => {
    const filteredTags = tags.filter((el) => el !== tag);
    setTags(filteredTags);
  };
  const updateUserName = (e:any) => {
    setUser(e.target.value);
  };
  const updateAddons = (e:any) => {
    setAddonName(e.target.value);
  };
  if (userData && userN.length === 0) {
    if (userData.Un) {
      const userNots = Object.keys(userData.Un).map((key) => {
        return userData.Un[key];
      });
      setUserN(userNots);
    } else {
      setUserN([-1]);
    }
  }
  if (userData && addonN.length === 0) {
    if (userData.An) {
      const unSeen: any[] = [];
      const addonNots = Object.keys(userData.An).map((key) => {
        if (userData.An[key][0] == '1') {
          unSeen.push(userData.An[key].slice(1));
        }
        return userData.An[key];
      });
      setAddonUnseeN(unSeen);
      setAddonN(addonNots);
    } else {
      setAddonN([-1]);
    }
  }
  return (
    <Main title='Admin'>
      <div className='w-11/12 mx-auto text-textW flex border border-fillerD my-2 px-4'>
        <div className="w-1/6 h-full my-2 border border-navbar">
          <img src=''></img>
          <h1 className="text-xl">Admin</h1>
          <div className='h-auto px-2'>
            <button value='notifications' onClick={handlePosition} className='w-full text-left my-0.5 py-1 focus:bg-navbar font-semibold focus:text-sGreen'>Notifications</button>
            <button value='users' onClick={handlePosition} className='w-full text-left  py-1 my-0.5 focus:bg-navbar font-semibold focus:text-sGreen'>Users</button>
            <button value='addons' onClick={handlePosition} className='w-full text-left my-0.5 py-1 focus:bg-navbar font-semibold focus:text-sGreen'>Addons</button>
            <button value='pendingAddons' onClick={handlePosition} className='w-full text-left my-0.5 py-1 font-semibold focus:bg-navbar focus:text-sGreen'>Pending Addons</button>
          </div>
        </div>
        <div className="w-5/6 flex-wrap h-fit">
          <div className='flex'>
            <form onSubmit={(e) => createTag(e)} className='w-11/12' noValidate>
              <label className='block mx-20 my-4 text-textW text-sm font-bold mb-2' htmlFor='tag'>Searching <span className='text-sGreen'>{prefix}</span><span className='text-red-400'> *</span></label>
              <div className='ml-20'>
                {tags ? tags.map((tag, key) => tag !== '' ? <div key={key} id='addon-tag' onClick={() => handleDeleteTag(tag)} className='inline-block bg-filler border border-filler rounded-full px-3 py-1 text-sm font-semibold text-textW cursor-pointer mr-2 mb-2 hover:bg-sGreen hover:text-mainBG'>{tag}</div> : <></>) : <></>}
              </div>
              <div className='flex'>
                {/* <img src={searchIcon} className='w-8 h-8 my-auto mx-3'></img> */}
                <select onChange={handlePrefix} className='bg-mainBG text-textW w-20 m-0 p-0 text-center' defaultValue={'Tags'}>
                  <option value='Tags'>Tags</option>
                  <option value='Users'>Users</option>
                  <option value='Addons'>Addons</option>
                </select>
                <input type='text' onChange={prefix === 'Tags' ? (e) => updateTags(e) : prefix === 'Users' ? (e) => updateUserName(e) : (e) => updateAddons(e)}
                  value={prefix === 'Tags' ? tag : prefix === 'Users' ? user : addonName} className='my-2 shadow appearance-none border-filler border rounded w-full py-2 px-3 text-textW bg-navbar leading-tight focus:outline-none focus:shadow-outline' name='tag' id='tag' required maxLength={20} minLength={2}></input>
              </div>
            </form>
            <div className='w-1/12 self-end'>
              <button className='mx-2 justify-self-center border-2 border-sGreen my-2 text-sGreen font-bold bg-mainBG hover:bg-sGreen font-bold rounded-lg text-sm px-5 py-2.5 text-center dark:bg-mainBG dark:hover:bg-sGreen hover:text-mainBG dark:focus:ring-sGreen alg' onClick={searchDB}>Search</button>
            </div>
          </div>
          <div className='my-2 w-fit mx-auto'>
            {userSearch && userSearch.length !== 0 ? <div className=''>
              <Link to={`/user/${userSearch.username}`} state={{creator: userSearch.username}}>
                <div className='border border-filler h-80'>
                  <div className='px-2'>
                    <img src={userSearch.logoUrl} className='object-fit mx-auto w-40 h-auto my-2'></img>
                  </div>
                  <div className='text-center'>
                    <h1 className={userSearch.type === 'blocked' ? 'font-semibold text-sRed' : userSearch.type === 'admin' ? 'font-semibold text-sBlue' : 'font-semibold text-sGreen'}><span className='text-sm'>Username: </span>{userSearch.username}</h1>
                    <h1>Email: {userSearch.email}</h1>
                    <h1>Phone: {userSearch.phoneNumber}</h1>
                  </div>
                </div>
              </Link>
            </div> : <></>}
            {searchAddons ? Object.keys(searchAddons).map((addonKey, key) => {
              const keyTyped = addonKey as keyof typeof searchAddons;
              const addon = searchAddons[keyTyped];
              return (
                <AddonCard2 key={key} {...addon}></AddonCard2>
              );
            }) : <div className='text-center w-full'>
              <h1 className='text-xl text-sRed'>No results found</h1>
            </div>}
          </div>
          {userSearch && searchAddons ? <></> :
        <div className=''>
          <div className='w-6/6 h-auto px-4'>
            {position === 'pendingAddons' ? <PendingAddons setPendingAddons={setPendingAddons}/> : position === 'users' ? <Users/>: position === 'addons' ? <ApprovedAddons addonType={'addons'}/> : position === 'notifications' ? <Notification addonN={addonN} userN={userN} user={userData.username}/> :<></>}
          </div>
        </div>
          }
        </div>
      </div>
    </Main>
  );
};

export default AdminPanel;
