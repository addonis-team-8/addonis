/* eslint-disable no-unused-vars */
import {useState, useEffect} from 'react';
import {getAllAddons} from '../../services/addon.service';
import AddonCard2 from '../AddonCard2';
import Pagination from '../Pagination';
const ApprovedAddons = (prop:any) => {
  const [addons, setAddons] = useState<any[]>([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [addonsPerPage, setAddonsPerPage] = useState(8);

  const getData = async () => {
    const data = await getAllAddons(prop.addonType);
    const data2 = await getAllAddons('featured');
    const bigData = {
      ...data,
      ...data2,
    };
    setAddons(bigData);
  };
  useEffect(() => {
    getData();
  }, []);
  const indexOfLastAddon = currentPage * addonsPerPage;
  const indexOfFirstAddon = indexOfLastAddon - addonsPerPage;
  const currentAddons = Object.fromEntries(
      Object.entries(addons).slice(indexOfFirstAddon, indexOfLastAddon),
  );
  const paginate = (pageNumber:any) => setCurrentPage(pageNumber);
  return (
    <div className=''>
      <div className='mx-auto my-5 grid grid-cols-1 br1:grid-cols-2 br2:grid-cols-3 br3:grid-cols-4 br4:grid-cols-5 gap-2'>
        {currentAddons ? Object.keys(currentAddons).map((addonKey, key) => {
          const keyTyped = addonKey as keyof typeof currentAddons;
          const addon = currentAddons[keyTyped];
          return (
            <AddonCard2 key={key} {...addon}></AddonCard2>
          );
        }) : <></>}
      </div>
      <div className='flex border border-fillerD justify-center '>
        <Pagination addonsPerPage={addonsPerPage} totalAddons={Object.keys(addons).length} paginate={paginate} currentPage={currentPage}></Pagination>
      </div>
    </div>
  );
};

export default ApprovedAddons;
