import {useContext, useEffect, useState} from 'react';
import {AppContext} from '../../providers/AppContext';
import {updateUserEmail} from '../../services/auth.service';
// import {useLocation} from 'react-router-dom';
import {updateUser} from '../../services/users.service';
import Relog from '../Relog/Relog';
import Swal from 'sweetalert2';
const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 2000,
  timerProgressBar: false,
  didOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer),
    toast.addEventListener('mouseleave', Swal.resumeTimer);
  },
});
const Edit = () => {
  const {user, userData, setContext} = useContext(AppContext);
  const [userD, setUser] = useState({...userData});
  const [userLogo, setUserLogo] = useState<any>();
  const [preview, setPreview] = useState('');
  const [previewName, setPreviewName] = useState('');
  const [logged, setLogged] = useState(true);
  const [uploadingBtn, setUploadingBtn] = useState(false);
  const update = (prop:any) => (event:any) => {
    setUser({
      ...userD,
      [prop]: event.target.value,
    });
  };
  const uploadUserLogo = (event:any) => {
    setUserLogo(event.target.files[0]);
  };
  const uploadChanges = async (e:any) => {
    e.preventDefault();
    if (JSON.stringify(userD) === JSON.stringify(userData) && !userLogo) {
      Swal.fire({
        title: 'Error',
        text: 'No changes have been made !',
        icon: 'warning',
        confirmButtonText: 'Ok',
        background: '#161a17',
      });
      return;
    }
    if (userD.email !== userData.email) {
      const result = await updateUserEmail('', userD.email);
      setLogged(result);
      if (!logged) {
        console.log('You will need to relog');
        setUploadingBtn(false);
      } else {
        setUploadingBtn(true);
        await updateUser(userD, userLogo);
      }
    } else {
      const res = await updateUser(userD, userLogo);
      if (res) {
        setContext({
          user: user,
          userData: userD,
        });
      }
      Toast.fire({
        icon: 'success',
        title: 'Updated successfully',
      });
    }
  };
  useEffect(() => {
    if (!userLogo) {
      setPreview('');
      setPreviewName('');
      return;
    }
    const userLogoUrl = URL.createObjectURL(userLogo);
    setPreview(userLogoUrl);
    setPreviewName(userLogo.name);
    return () => URL.revokeObjectURL(userLogoUrl);
  }, [userLogo]);

  return (
    <div className='w-full h-80'>
      <h1>Account Details</h1>
      {!logged ? <Relog /> : <></>}
      <form onSubmit={(e) => uploadChanges(e)}>
        <div className='w-6/6 mx-auto'>
          <div className="md:flex md:items-center mb-2">
            <div className=" w-1/6">
              <label className="block text-textW font-bold md:text-right mb-1 md:mb-0 px-2" htmlFor="Email">Change Email: </label>
            </div>
            <div className="md:w-2/3">
              <input id='Email' onChange={update('email')} value={userD.email} className="my-2 shadow appearance-none border-filler border rounded w-full py-2 px-3 text-textW leading-tight focus:outline-none focus:shadow-outline bg-navbar" type="email" ></input>
            </div>
          </div>
          <div className="md:flex md:items-center mb-2">
            <div className=" w-1/6">
              <label className="block text-textW font-bold md:text-right mb-1 md:mb-0 px-2" htmlFor="phone">Change Phone: </label>
            </div>
            <div className="md:w-2/3">
              <input id='phone' onChange={update('phoneNumber')} value={userD.phoneNumber} className="my-2 shadow appearance-none border-filler border rounded w-full py-2 px-3 text-textW leading-tight focus:outline-none focus:shadow-outline bg-navbar" type="phone" minLength={10} maxLength={10}></input>
            </div>
          </div>
          {preview ? <div className='text-textW flex my-2 mx-auto'>
            <img src={preview} className='w-32 h-32 mx-auto transition-all ease-in duration-300'></img>
          </div> : <></>}
          <div className='md:flex md:items-center mb-2'>
            <div className='w-1/6'>
              <label className='block text-textW font-bold md:text-right mb-1 md:mb-0 px-2' htmlFor='file_input'>Change Avatar:</label>
            </div>
            <div className='flex justify-center items-center md:w-2/3'>
              <label htmlFor='uploadImg' className='flex flex-col justify-center items-center w-full h-32 bg-gray-50 rounded-lg border-2 border-textW cursor-pointer dark:hover:bg-bray-800 dark:bg-gray-700 hover:bg-gray-100 dark:border-gray-600 dark:hover:border-fillerD dark:hover:bg-gray-600'>
                <div className='flex flex-col justify-center items-center'>
                  <div className={previewName ? 'text-dGreen w-8 h-8' : 'text-gray-500 w-8 h-8'}>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox='0 0 24 24' className='fill-current'>
                      <g fillRule="evenodd">
                        <path d='M3 21a1 1 0 01-1-1V6a1 1 0 011-1h10a1 1 0 010 2H4v9.271l1.879-1.878a1 1 0 011.32-.083l.094.083 1.619 1.62L6.084 19h2.754l5.862-6.192a1 1 0 011.413-.039l.038.038L18 14.973V12a1 1 0 012 0v8a1 1 0 01-1 1H3zm7.5-12a1.5 1.5 0 110 3 1.5 1.5 0 010-3z'></path>
                        <path d='M19 3a1 1 0 011 1l-.001 1H21a1 1 0 010 2h-1.001L20 8a1 1 0 01-2 0l-.001-1H17a1 1 0 010-2h.999L18 4a1 1 0 011-1z'></path>
                      </g>
                    </svg>
                  </div>
                  <p className='m-0 text-sm text-gray-500 dark:text-gray-400'><span className='font-semibold'>{previewName ? `${previewName}` : 'Click to upload or drag and drop'}</span></p>
                  <p className="m-0 text-sm text-gray-500 dark:text-gray-300" id="file_input_help">{previewName ? '' : 'SVG, PNG, JPG or GIF.'}</p>
                </div>
                <input id='uploadImg' type='file' className='hidden' onChange={uploadUserLogo} accept='image/*'/>
              </label>
            </div>
          </div>
        </div>
        <div className='border w-3/6 mx-auto text-center'>
          {!uploadingBtn ? <button type='submit' className='justify-self-center border-2 border-sGreen my-2 text-sGreen font-bold bg-mainBG hover:bg-sGreen focus:ring-4 focus:outline-none focus:ring-sGreen font-bold rounded-lg text-sm px-5 py-2.5 text-center dark:bg-mainBG dark:hover:bg-sGreen hover:text-mainBG dark:focus:ring-sGreen alg'>Save Changes</button> :
          <button className='justify-self-center text-mainBG bg-sGreen hover:bg-sGreen focus:ring-4 focus:outline-none focus:ring-sGreen font-medium rounded-lg text-sm pr-5 py-2.5 my-2 text-center dark:bg-sGreen dark:hover:bg-sGreen dark:focus:ring-sGreen alg'><div className="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>Saving Changes</button>}
        </div>
      </form>
    </div>
  );
};

export default Edit;
