/* eslint-disable no-unused-vars */
import React, {useEffect, useState} from 'react';
import {GiHamburgerMenu} from 'react-icons/gi';
import {ImCross} from 'react-icons/im';
import {Link, NavLink} from 'react-router-dom';
import {AppContext} from '../../providers/AppContext';
import {useContext} from 'react';
import {logoutUser} from '../../services/auth.service';
import {setSeenNotifications} from '../../services/notification.service';
import {getAddon} from '../../services/addon.service';
import {useNavigate} from 'react-router-dom';

const Header = () => {
  const [showButton, setShowButton] = useState(true);
  const [notShow, setNotShow] = useState(false);
  const [userN, setUserN] = useState<any[]>([]);
  const [addonN, setAddonN] = useState<any[]>([]);
  const [addoUnseenN, setAddonUnseeN] = useState<any[]>([]);
  const [userUnseeN, setUserUnseeN] = useState<any[]>([]);
  const [addonOrNot, setAddonOrNot] = useState(true);
  const {user, userData, setContext} = useContext(AppContext);
  const handleClick = () => {
    showButton ? setShowButton(false) : setShowButton(true);
  };
  const navigate = useNavigate();
  const logout = () => {
    logoutUser()
        .then(() => {
          setContext({user: undefined, userData: null});
          navigate('/');
        });
  };
  const handleShowNotifications = () => {
    setNotShow(true);
  };
  const handleCleanNotifications = () => {
    setNotShow(false);
  };

  const getAn = () => {
    if (userData.An) {
      const unSeen: any[] = [];
      const addonNots = Object.keys(userData.An).map((key) => {
        if (userData.An[key][0] == '1') {
          unSeen.push(userData.An[key].slice(1).split('|'));
        }
        return userData.An[key];
      });
      setAddonUnseeN(unSeen);
      setAddonN(addonNots);
    } else {
      setAddonN([-1]);
    }
  };
  const getUn = () => {
    if (userData.Un) {
      const unSeen: any[] = [];
      const userNots = Object.keys(userData.Un).map((key) => {
        if (userData.Un[key][0] == '1') {
          unSeen.push(userData.Un[key].slice(1).split('|'));
        }
        return userData.Un[key];
      });
      setUserUnseeN(unSeen);
      setUserN(userNots);
    } else {
      setUserN([-1]);
    }
  };
  if (userData && userData.An && Object.keys(userData.An).length !== addonN.length) {
    getAn();
  } else if (userData && !userData.An && addonN.length !== 0) {
    setAddonN([]);
    setAddonUnseeN([]);
  }
  if (userData && userData.Un && Object.keys(userData.Un).length !== userN.length) {
    getUn();
  } else if (userData && !userData.Un && userN.length !== 0) {
    setUserN([]);
    setUserUnseeN([]);
  }
  const handleNot = () => {
    setAddonOrNot(!addonOrNot);
  };
  const handleUserSeen = () => {
    setUserUnseeN([]);
    setSeenNotifications(userData.username, userN, 'Un');
  };
  const handleAddonSeen = () => {
    setAddonUnseeN([]);
    setSeenNotifications(userData.username, addonN, 'An');
  };
  const handleSeeAll = () => {
    // setUserUnseeN([]);
    // setAddonUnseeN([]);
    handleUserSeen();
    handleAddonSeen();
  };
  const allUnsenLenght = userUnseeN.length + addoUnseenN.length;
  const handleAddon = async (addonName:any) => {
    const data = await getAddon(addonName);
    navigate(`/addon/${data.name}`, {state: {addon: {...data}}});
  };
  return (user ?
    <nav className='px-8 py-4 shadow md:flex md:items-center md:justify-between bg-navbar text-textW'>
      <div className='flex justify-between items-center'>
        <span className='text-2xl font-mono cursor-pointer font-semibold'><NavLink to={'/'}>Addonis</NavLink></span>
        <span className='text-3xl cursor-pointer md:hidden block' id='1'>
          {showButton ? <GiHamburgerMenu onClick={handleClick}/> : <ImCross onClick={handleClick}/>}
        </span>
      </div>
      <ul className={showButton ?
      `md:flex md:items-center z-[-1] md:z-auto md:static absolute
       bg-navbar text-textW w-full left-0 md:w-auto md:py-0 py-4 md:pl-0 pl-7 md:opacity-100
        opacity-0 top-[-400px] transition-all ease-in duration-300` :
        `md:flex md:items-center z-10 md:z-auto md:static absolute
        bg-sky-200 w-full left-0 md:w-auto md:py-0 py-4 md:pl-0 pl-7 md:opacity-100
         opacity-100 top-[80px] transition-all ease-in duration-300`}>
        {/* <li className='mx-4 my-6 md:my-0 hover:underline hover:decoration-dGreen underline-offset-8 decoration-2'><NavLink to={'/home'}>Home</NavLink></li>
        <li className='mx-4 my-6 md:my-0 hover:underline hover:decoration-dGreen underline-offset-8 decoration-2'><NavLink to={'/about'}>About</NavLink></li> */}
        {notShow ? <div className='text-textW absolute top-10 w-80 h-80' onMouseEnter={handleShowNotifications} onMouseLeave={handleCleanNotifications}>
          <div className='border-2 border-fillerD my-4 bg-navbar'>
            <div className='border border-mainBG w-full py-1'>
              <button className={addonOrNot ? 'w-3/6 text-dGreen' : 'w-3/6 hover:text-dGreen'} onClick={handleNot}>Addons</button>
              <button className={!addonOrNot ? 'w-3/6 text-dGreen' : 'w-3/6 hover:text-dGreen'} onClick={handleNot}>Users</button>
            </div>
            {addonOrNot ? <div>
              {addoUnseenN && addoUnseenN.length > 0 ? addoUnseenN.map((addon, key) => {
                return (
                  <div key={key}>
                    <h1 className='border border-fillerD px-2 py-1 text-sm text-gray-400'>
                      <Link to={`/user/${addon[0]}`} state={{creator: addon[0]}}><span className='text-dGreen hover:cursor-pointer'>{addon[0]} </span></Link>
                      <span>{addon[1]} </span>
                      <button className='text-textW hover:text-sBlue' onClick={() => handleAddon(addon[2])}><span> {addon[2]}</span></button>
                    </h1>
                  </div>
                );
              }) : <h1 className='w-full font-bold'>Nothing new...</h1>}
            </div> : <div>
              {userUnseeN && userUnseeN.length > 0 ? userUnseeN.map((user, key) => {
                return (
                  <div key={key}>
                    <h1 className='border border-fillerD px-2 py-1 text-sm text-gray-400'>
                      <Link to={`/user/${user[0]}`} state={{creator: user[0]}}><span className='text-dGreen hover:cursor-pointer'>{user[0]} </span></Link>
                      <span>{user[1]} </span>
                      {/* <Link to=''><span className='hover:pointer-cursor text-sBlue'>{user[2]}</span></Link> */}
                      {/* <Link to={`/addon/${addon[2]}`} state={{addon: addon[2]}}><span>{addon[2]}</span></Link> */}
                    </h1>
                  </div>
                );
              }) : <h1 className='w-full font-bold'>Nothing new...</h1>}
            </div>}
          </div>
        </div> : <></>}
        {/* <span className="relative inline-block">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox='0 0 24 24' className='fill-current'>
            <path d='M15 19a2 2 0 01-2 2h-2a2 2 0 01-2-2h6zM13 2a1 1 0 011 1v1h2.656c.429 0 .835.41.907.917L19 13l1.707 1.707a1 1 0 01.293.707V17a1 1 0 01-1 1H4a1 1 0 01-1-1v-1.586a1 1 0 01.293-.707L5 13l1.437-8.083C6.51 4.41 6.915 4 7.344 4H10V3a1 1 0 011-1h2z'></path>
          </svg>
          <span className="absolute top-0 right-0 inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-red-100 transform translate-x-1/2 -translate-y-1/2 bg-red-600 rounded-full">99</span>
        </span> */}
        <div className='hover:cursor-pointer' onMouseEnter={handleShowNotifications} onMouseLeave={handleCleanNotifications} onClick={handleSeeAll}>
          <div className={notShow ? 'w-6 h-6 mt-2 mr-1 text-sGreen' :'w-6 h-6 mt-2 mr-1 hover:text-sGreen'}>
            <span className="relative inline-block">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox='0 0 24 24' className='fill-current'>
                <path d='M15 19a2 2 0 01-2 2h-2a2 2 0 01-2-2h6zM13 2a1 1 0 011 1v1h2.656c.429 0 .835.41.907.917L19 13l1.707 1.707a1 1 0 01.293.707V17a1 1 0 01-1 1H4a1 1 0 01-1-1v-1.586a1 1 0 01.293-.707L5 13l1.437-8.083C6.51 4.41 6.915 4 7.344 4H10V3a1 1 0 011-1h2z'></path>
              </svg>
              {allUnsenLenght > 0 ? <span className="absolute top-0 right-0 inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-textW transform translate-x-1/2 -translate-y-1/2 bg-red-600 rounded-full">{allUnsenLenght}</span> : <></>}
            </span>
          </div>
        </div>
        <li className='mx-4 my-6 md:my-0 hover:underline hover:decoration-sGreen hover:cursor-pointer underline-offset-8 decoration-2 text-sBlue' onClick={logout}>Logout</li>
        {/* <li className='mx-4 my-6 md:my-0 hover:underline hover:decoration-dGreen underline-offset-8 decoration-2'><NavLink to={'/contact'}>Nots</NavLink></li> */}
        {userData.type !== 'admin' ? <NavLink className='mx-4 my-6 md:my-0 hover:underline hover:decoration-sGreen underline-offset-8 decoration-2' to={`/profile`}>{userData.username}</NavLink> :
         <NavLink className='mx-4 my-6 md:my-0 hover:underline hover:decoration-sGreen underline-offset-8 decoration-2' to={`/admin`}>{userData.username}</NavLink> }
        <li className={userData.type === 'blocked' ? 'mx-4 my-6 md:my-0 hover:underline hover:decoration-sRed underline-offset-8 decoration-2 text-sRed' : 'mx-4 my-6 md:my-0 hover:underline hover:decoration-sGreen underline-offset-8 decoration-2 text-sGreen'}>{userData.type !== 'blocked' ? <NavLink to={'/upload'}>Upload</NavLink> : 'Upload'}</li>
      </ul>
    </nav>:

    <nav className='px-8 py-4 shadow md:flex md:items-center md:justify-between bg-navbar text-textW'>
      <div className='flex justify-between items-center'>
        <span className='text-2xl font-mono cursor-pointer font-semibold'><NavLink to={'/'}>Addonis</NavLink></span>
        <span className='text-3xl cursor-pointer md:hidden block' id='1'>
          {showButton ? <GiHamburgerMenu onClick={handleClick}/> : <ImCross onClick={handleClick}/>}
        </span>
      </div>
      <ul className={showButton ?
      `md:flex md:items-center z-[-1] md:z-auto md:static absolute
       bg-navbar text-textW w-full left-0 md:w-auto md:py-0 py-4 md:pl-0 pl-7 md:opacity-100
        opacity-0 top-[-400px] transition-all ease-in duration-300` :
        `md:flex md:items-center z-10 md:z-auto md:static absolute
        bg-sky-200 w-full left-0 md:w-auto md:py-0 py-4 md:pl-0 pl-7 md:opacity-100
         opacity-100 top-[80px] transition-all ease-in duration-300`}>
        <li className='mx-4 my-6 md:my-0 hover:underline hover:decoration-sGreen underline-offset-8 decoration-2'><NavLink to={'/home'}>Home</NavLink></li>
        <li className='mx-4 my-6 md:my-0 hover:underline hover:decoration-sGreen underline-offset-8 decoration-2'><NavLink to={'/register'}>Register</NavLink></li>
        <li className='mx-4 my-6 md:my-0 hover:underline hover:decoration-sGreen underline-offset-8 decoration-2'><NavLink to={'/login'}>Login</NavLink></li>
      </ul>
    </nav>
  );
};

export default Header;
