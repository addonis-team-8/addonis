import React, {useContext, useState} from 'react';
import Main from '../Main/Main';
import {loginUser} from '../../services/auth.service';
import {getUserData} from '../../services/users.service';
import {AppContext} from '../../providers/AppContext';
import {useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import LoginButton from '../LoginButton/LoginButton';
import Loading from '../Loading/Loading';
// import Swal from 'sweetalert2';
const Login = () => {
  const userObj = {
    email: '',
    password: '',
  };
  const [user, setUser] = useState({...userObj});
  const [isLoading, setIsLoading] = useState(false);
  const {setContext} = useContext(AppContext);
  const navigate = useNavigate();
  const update = (prop:any) => (event:any) => {
    setUser({
      ...user,
      [prop]: event.target.value,
    });
  };
  const handleLogin = async (e:any) => {
    e.preventDefault();
    try {
      setIsLoading(true);
      const loginData = await loginUser(user.email, user.password);
      const userData = await getUserData(loginData.user.uid);
      setContext({
        user: loginData.user,
        userData: userData.val()[Object.keys(userData.val())[0]],
      });
      navigate('/');
    } catch (err:any) {
      setIsLoading(false);
      Swal.fire({
        title: 'Error',
        text: 'Wrong credentials',
        icon: 'error',
        confirmButtonText: 'Ok',
        background: '#161a17',
        color: '#f8f8f8',
      });
    }
  };
  return (
    <Main title='Login'>
      <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <div className="max-w-md w-full space-y-8">
          <div>
            <img className="mx-auto h-12 w-auto" src="https://tailwindui.com/img/logos/workflow-mark-blue-600.svg" alt="Workflow"/>
            <h2 className="mt-6 text-center text-3xl font-extrabold text-textW">Login</h2>
          </div>
          <form className="mt-8 space-y-6" action="#" method="POST" onSubmit={(e) => handleLogin(e)}>
            <input type="hidden" name="remember" value="true"/>
            <div className="rounded-md shadow-sm -space-y-px">
              <div>
                <label htmlFor="email-address" className="sr-only">Email address</label>
                <input id="email-address" onChange={update('email')} name="email" type="email" autoComplete="email" required className="appearance-none rounded-none relative block w-full px-3 py-2 border-2 border-textG placeholder-gray-500 text-textW rounded-t-md my-2 focus:outline-none focus:ring-textW focus:border-sky-500 focus:bg-fillerD focus:z-10 sm:text-sm bg-fillerD" placeholder="Email address"/>
              </div>
              <div>
                <label htmlFor="password" className="sr-only">Password</label>
                <input id="password" onChange={update('password') }name="password" type="password" autoComplete="current-password" required className="appearance-none rounded-none relative block w-full px-3 py-2 border-2 border-textG placeholder-gray-500 text-textW rounded-t-md my-2 focus:outline-none focus:ring-textW focus:border-sky-500 focus:bg-fillerD sm:text-sm bg-fillerD" placeholder="Password"/>
              </div>
            </div>
            <div>
              {isLoading ? <Loading/> : <LoginButton/>}
            </div>
          </form>
        </div>
      </div>
    </Main>
  );
};

export default Login;
