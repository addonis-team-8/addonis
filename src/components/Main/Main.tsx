import React, {useEffect} from 'react';

type Props = {
  title: string,
  children: React.ReactNode
};

const Main = ({title, children}:Props) => {
  useEffect(() => {
    document.title = `${title} | Addonis`;
  }, []);

  return (
    <>
      {children}
    </>
  );
};

export default Main;
