import React from 'react';
import {NavLink} from 'react-router-dom';
import Main from '../Main/Main';
import './NotFound.css';

const NotFound = () => {
  return (
    <Main title={'404 Not Found'}>
      <h1 className={'not-found'}>404 Page Not Found</h1>
      <NavLink to={'/'} className={'go-home'}>Go back to the home page.</NavLink>
    </Main>
  );
};

export default NotFound;
