/* eslint-disable no-unused-vars */
import {useEffect, useState} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import {getAddon} from '../../services/addon.service';
import {clearAllNotifications} from '../../services/notification.service';
const Notification = ({addonN, userN, user}:any) => {
  const [addonNotifications, setAddonNotifications] = useState<any>();
  const [userNotifications, setUserNotifications] = useState<any>();
  const [userDel, setUserDel] = useState(false);
  const [addonDel, setAddonDel] = useState(false);
  const navigate = useNavigate();
  const decypherAn = (addonN:any, type:string) => {
    const nots = Object.keys(addonN).map((key) => {
      return (addonN[key].slice(1).split('|'));
    });
    if (type === 'addons') {
      setAddonNotifications(nots);
    } else {
      setUserNotifications(nots);
    }
  };
  useEffect(() => {
    if (addonN.length > 1) {
      decypherAn(addonN, 'addons');
    };
    if (userN.length > 1) {
      decypherAn(userN, 'user');
    };
  }, []);
  const handleAddon = async (addonName:any) => {
    const data = await getAddon(addonName);
    navigate(`/addon/${data.name}`, {state: {addon: {...data}}});
  };
  const handleMouse = (which:string) => {
    if (which === 'user') {
      setUserDel(true);
    } else if (which === 'userOut') {
      setUserDel(false);
    }
    if (which === 'addon') {
      setAddonDel(true);
    } else if (which === 'addonOut') {
      setAddonDel(false);
    }
  };
  const handleDeleteNots = (which:string) => {
    clearAllNotifications(user, which);
    if (which == 'An') {
      setAddonNotifications([]);
    } else if (which === 'Un') {
      setAddonNotifications([]);
    }
  };
  return (<div className="border border-navbar h-80 flex-wrap">
    <div className='flex'>
      <div className='border border-fillerD w-3/6'>
        <div className=''>
          <h1 className='border border-fillerD text-center italic hover:text-sRed hover:cursor-pointer' onMouseEnter={() => handleMouse('addon')} onMouseOut={() => handleMouse('addonOut')} onClick={() => handleDeleteNots('An')}>{addonDel ? 'Clear Addon Notifications' :'Addon Related'}</h1>
        </div>
        {addonNotifications ? addonNotifications.map((addon:any, key:any) => {
          return (
            <div key={key} className='border border-navbar px-4 my-2 mx-2 text-center'>
              <Link to={`/user/${addon[0]}`} state={{creator: addon[0]}}><span className='font-semibold text-xl text-dGreen hover:cursor-pointer'>{addon[0]} </span></Link>
              <span className='text-gray-400 italic'>{addon[1]} </span>
              <button className='text-textW hover:text-sBlue' onClick={() => handleAddon(addon[2])}><span> {addon[2]}</span></button>
            </div>
          );
        }) : <h1 className='text-center mt-20 font-bold'>Nothing new...</h1>}
      </div>
      <div className='border border-fillerD w-3/6'>
        <div className=''>
          <h1 className='border border-fillerD text-center italic hover:text-sRed hover:cursor-pointer' onMouseEnter={() => handleMouse('user')} onMouseOut={() => handleMouse('userOut')} onClick={() => handleDeleteNots('Un')}>{userDel ? 'Clear User Notifications' :'User Related'}</h1>
        </div>
        {userNotifications ? userNotifications.map((addon:any, key:any) => {
          return (
            <div key={key} className='border border-navbar px-4 my-2 mx-2 text-center'>
              <Link to={`/user/${addon[0]}`} state={{creator: addon[0]}}><span className='font-semibold text-xl text-dGreen hover:cursor-pointer'>{addon[0]} </span></Link>
              <span className='text-gray-400 italic'>{addon[1]} </span>
            </div>
          );
        }) : <h1 className='text-center mt-20 font-bold'>Nothing new...</h1>}
      </div>
    </div>
  </div>);
};

export default Notification;
