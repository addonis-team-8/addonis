
const Pagination = ({addonsPerPage, totalAddons, paginate, currentPage}:any) => {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalAddons / addonsPerPage); i++) {
    pageNumbers.push(i);
  }
  return (
    <div className="border-navbar flex">
      {pageNumbers.map((number, key) => (
        <div key={key} className='border border-filler mx-0.5 hover:cursor-pointer hover:bg-sBlue' onClick={() => paginate(number)}>
          <p className={number == currentPage ? 'w-fit font-bold text-sGreen' : 'w-fit font-bold text-textW hover:text-sGreen'}>
            {number}
          </p>
        </div>
      ))}
    </div>
  );
};
export default Pagination;
