import {useEffect, useState} from 'react';
import {getAllAddons} from '../../services/addon.service';
// import AddonCard from '../AddonCard';
import AddonCard2 from '../AddonCard2';
import Pagination from '../Pagination';
const PendingAddons = ({setPendingAddons}:any) => {
  const [addons, setAddons] = useState<any[]>([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [addonsPerPage] = useState(8);
  const getAddons = async () => {
    const data = await getAllAddons('pending');
    setAddons(data);
    setPendingAddons(data);
  };
  useEffect(() => {
    getAddons();
  }, []);
  console.log('pending', addons);
  const indexOfLastAddon = currentPage * addonsPerPage;
  const indexOfFirstAddon = indexOfLastAddon - addonsPerPage;
  let currentAddons:any;
  if (addons && Object.keys(addons).length >= 1) {
    currentAddons = Object.fromEntries(
        Object.entries(addons).slice(indexOfFirstAddon, indexOfLastAddon),
    );
  }

  const paginate = (pageNumber:any) => setCurrentPage(pageNumber);
  return (
    <div className=''>
      <div className='mx-auto my-5 grid grid-cols-1 br1:grid-cols-2 br2:grid-cols-3 br3:grid-cols-4 br4:grid-cols-5 gap-2'>
        {currentAddons ? Object.keys(currentAddons).map((addonKey, key) => {
          const keyTyped = addonKey as keyof typeof currentAddons;
          const addon = currentAddons[keyTyped];
          return (
            <AddonCard2 key={key} {...addon}></AddonCard2>
          );
        }) : <></>}
      </div>
      <div className='flex border border-fillerD justify-center '>
        {currentAddons ? <Pagination addonsPerPage={addonsPerPage} totalAddons={Object.keys(addons).length} paginate={paginate} currentPage={currentPage}></Pagination> : <></> }
      </div>
    </div>
  );
};

export default PendingAddons;

