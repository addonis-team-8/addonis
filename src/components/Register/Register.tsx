import React, {useContext, useState} from 'react';
import Main from '../Main/Main';
import {loginUser, registerUser} from '../../services/auth.service';
import {createUser, getUserData, uploadLogo, userCheck} from '../../services/users.service';
import Swal from 'sweetalert2';
import {useNavigate} from 'react-router-dom';
import {AppContext} from '../../providers/AppContext';
import {createNotification} from '../../services/notification.service';
import RegisterButton from '../RegisterButton/RegisterButton';
import Loading from '../Loading/Loading';
const Register = () => {
  const userObj = {
    username: '',
    email: '',
    password: '',
    phone: '',
    photoLink: '',
  };
  // eslint-disable-next-line quotes
  const emailRegex = new RegExp("([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])");
  const [user, setUser] = useState({...userObj});
  const [userLogoFile, setUserLogoFile] = useState('');
  const [loadingState, setLoadingState] = useState(false);
  const navigate = useNavigate();
  const {setContext} = useContext(AppContext);
  const update = (prop:any) => (event:any) => {
    if (prop == 'phone') {
      event.target.value = event.target.value.replace(/\D\w/g, '');
    };
    if (prop == 'username') {
      event.target.value = event.target.value.replace(/[^a-zA-Z0-9]/g, '');
    };
    setUser({
      ...user,
      [prop]: event.target.value,
    });
  };
  const uploadUserLogo = (e:any) => {
    setUserLogoFile(e.target.files[0]);
  };
  const handleLogin = async (e:any) => {
    e.preventDefault();
    const loginData = await loginUser(user.email, user.password);
    const userData = await getUserData(loginData.user.uid);
    setContext({
      user: loginData.user,
      userData: userData.val()[Object.keys(userData.val())[0]],
    });
    navigate('/');
  };
  const handleRegister = async (e:any) => {
    // CHECKS then:
    e.preventDefault();
    if (user.username.length < 2 || user.username.length > 20) {
      return Swal.fire({
        title: 'Error',
        text: 'Username must be between 2 and 20 characters',
        icon: 'error',
        confirmButtonText: 'Ok',
        background: '#161a17',
        color: '#f8f8f8',
      });
    }
    const validEmail = emailRegex.test(user.email);
    const check = await userCheck(user);
    if (!check && validEmail) {
      setLoadingState(true);
      const registrationData = await registerUser(user.email, user.password);
      const userLogoData = await uploadLogo(userLogoFile);
      createUser(user.username, registrationData.user.uid, user.email, user.phone, userLogoData.userLogoUrl, userLogoData.userLogoName);
      handleLogin(e);
      createNotification('admin', `${user.username}|just joined!`, 'Un');
    } else {
      setLoadingState(false);
      Swal.fire({
        title: 'Error',
        text: check,
        icon: 'error',
        confirmButtonText: 'Ok',
        background: '#161a17',
        color: '#f8f8f8',
      });
    }
  };
  return (
    <Main title='Register'>
      <div>
        <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
          <div className="max-w-md w-full space-y-8">
            <div>
              <img className="mx-auto h-12 w-auto" src="https://tailwindui.com/img/logos/workflow-mark-blue-600.svg" alt="Workflow"/>
              <h2 className="mt-6 text-center text-3xl font-extrabold text-textW">Register</h2>
            </div>
            <form className="mt-8 space-y-6" onSubmit={(e) => handleRegister(e)}>
              <input type="hidden" name="remember" value="true"/>
              <div className="rounded-md shadow-sm -space-y-px">
                <div>
                  <label htmlFor="name" className="sr-only">Username</label>
                  <input id="name" onChange={update('username')} name="name" type="text" autoComplete="email" required className="appearance-none rounded-none relative block w-full px-3 py-2 border-2 border-textG placeholder-gray-500 text-textW rounded-t-md my-2 focus:outline-none focus:ring-textW focus:border-sky-500 focus:bg-filledD focus:z-10 sm:text-sm bg-fillerD" placeholder="Username" minLength={2} maxLength={20}/>
                </div>
                <div>
                  <label htmlFor="email-address" className="sr-only">Email address</label>
                  <input id="email-address" onChange={update('email')} name="email" type="email" autoComplete="email" required className="appearance-none rounded-none relative block w-full px-3 py-2 border-2 border-textG placeholder-gray-500 text-textW rounded-t-md my-2 focus:outline-none focus:ring-textW focus:border-sky-500 focus:bg-filledD focus:z-10 sm:text-sm bg-fillerD" placeholder="Email address"/>
                </div>
                <div>
                  <label htmlFor="password" className="sr-only">Password</label>
                  <input id="password" onChange={update('password')} name="password" type="password" autoComplete="current-password" required className="appearance-none rounded-none relative block w-full px-3 py-2 border-2 border-textG placeholder-gray-500 text-textW rounded-t-md my-2 focus:outline-none focus:ring-textW focus:border-sky-500 focus:bg-filledD focus:z-10 sm:text-sm bg-fillerD" placeholder="Password" minLength={6}/>
                </div>
                <div>
                  <label htmlFor="phone" className="sr-only">Phone Number</label>
                  <input id="phone" onChange={update('phone')} name="phone" type="text" autoComplete="current-password" required className="appearance-none rounded-none relative block w-full px-3 py-2 border-2 border-textG placeholder-gray-500 text-textW rounded-t-md my-2 focus:outline-none focus:ring-textW focus:border-sky-500 focus:bg-filledD focus:z-10 sm:text-sm bg-fillerD" placeholder="Phone Number" minLength={10} maxLength={10}/>
                </div>
              </div>
              <div>
                <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300" htmlFor="file_input">Upload file</label>
                <input onChange={uploadUserLogo} className="appearance-none rounded-none relative block w-full px-3 py-2 border-2 border-textG placeholder-gray-500 text-textW rounded-t-md my-2 focus:outline-none focus:ring-textW focus:border-sky-500 focus:bg-filledD focus:z-10 sm:text-sm bg-fillerD" id="file_input" type="file" accept='image/*'></input>
              </div>
              <div className='text-center'>
                {loadingState ? <Loading/> : <RegisterButton/>}
              </div>
            </form>
          </div>
        </div>
      </div>
    </Main>
  );
};

export default Register;
