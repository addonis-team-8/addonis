import {useState} from 'react';
import {loginUser} from '../../services/auth.service';
const Relog = () => {
  const userObj = {
    email: '',
    password: '',
  };
  const [user, setUser] = useState({...userObj});
  const update = (prop:any) => (event:any) => {
    setUser({
      ...user,
      [prop]: event.target.value,
    });
  };
  const handleLogin = async (e:any) => {
    e.preventDefault();
    const res = await loginUser(user.email, user.password);
    console.log('res for logging in', res);
  };
  return (
    <form onSubmit={(e) => handleLogin(e)}>
      <div className="rounded-md shadow-sm -space-y-px border border-filler w-3/6 mx-auto px-8 py-4">
        <div className='text-sRed text-center font-semibold'>
          <h1>You will have to relog to save your changes !</h1>
        </div>
        <div>
          <label htmlFor="email-address" className="sr-only">Old Email address</label>
          <input id="email-address" onChange={update('email')} name="email" type="email" autoComplete="email" required className="appearance-none rounded-none relative block w-full px-3 py-2 border-2 border-textG placeholder-gray-500 text-textW rounded-t-md my-2 focus:outline-none focus:ring-textW focus:border-sky-500 focus:bg-fillerD focus:z-10 sm:text-sm bg-fillerD" placeholder="Old Email address"/>
        </div>
        <div>
          <label htmlFor="password" className="sr-only">Password</label>
          <input id="password" onChange={update('password') }name="password" type="password" autoComplete="current-password" required className="appearance-none rounded-none relative block w-full px-3 py-2 border-2 border-textG placeholder-gray-500 text-textW rounded-t-md my-2 focus:outline-none focus:ring-textW focus:border-sky-500 focus:bg-fillerD sm:text-sm bg-fillerD" placeholder="Password"/>
        </div>
        <div className='text-center'>
          <button type='submit' className="text-dGreen border-2 border-dGreen px-6 py-2 rounded-lg hover:bg-dGreen hover:text-mainBG font-semibold">Relog</button>
        </div>
      </div>
    </form>
  );
};
export default Relog;
