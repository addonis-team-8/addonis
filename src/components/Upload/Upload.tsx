/* eslint-disable no-unused-vars */
/* eslint-disable react/no-unknown-property */
import React, {useState, useContext, useEffect} from 'react';
import {useNavigate} from 'react-router-dom';
import {addonCheck, addonCheck2, createApiAndGetRepo, getLastCommit, updateAddon, uploadAddon, uploadAddonLogo, uploadToFirebase} from '../../services/addon.service';
import Swal from 'sweetalert2';
import {AppContext} from '../../providers/AppContext';
import toast from '../Toast/toaster';
import {createNotification} from '../../services/notification.service';
type ChildProps = {
  addon: any,
  handleEditing: (e: React.ChangeEvent<HTMLInputElement>, prop:string) => void
}
const Upload = (props:ChildProps) => {
  const addonObj = {
    name: '',
    description: '',
    creator: '',
    tags: [''],
    downloads: 0,
    ratings: 0,
    downloadLink: '',
    originLink: '',
    apiLink: '',
    issues: '',
    pulls: '',
    lastCommit: '',
    uploadDate: '',
    approved: false,
    addonLogo: '',
    fileName: '',
    logoName: '',
    ide: '',
    featured: false,
  };
  const fileObj = {
    name: '',
    lastModified: 0,
    size: 0,
    webkitRelativePath: '',
  };
  const [addonData, setAddonData] = useState({...addonObj});
  const [adddonRawData, setAddonRawData] = useState({...fileObj});
  const [addonLogoFile, setAddonLogo] = useState({...fileObj});
  const [tag, setTag] = useState('');
  const [tags, setTags] = useState(['']);
  const [oldAddonName, setOldAddonName] = useState('');
  const [uploadingBtn, setUploadingBtn] = useState(false);
  const {userData} = useContext(AppContext);
  const navigate = useNavigate();

  if (userData?.type === 'blocked') {
    Swal.fire({
      title: 'Error',
      text: 'Sorry you are currently blocked from uploading',
      icon: 'error',
      confirmButtonText: 'Ok',
      background: '#242a2f',
      color: 'fff',
    });
    navigate('/');
  };

  useEffect(() => {
    if (Object.keys(props.addon).length > 5) {
      setAddonData({...props.addon});
      setTags([...props.addon.tags]);
      setOldAddonName(props.addon.name);
    } else {
      setAddonData({...addonObj});
    }
  }, []);

  const updateUploadFile = (event:any) => {
    const extension = event.target.files[0].name.split('.').pop();
    if (extension !== 'vsix') {
      return Swal.fire({
        title: 'Error',
        text: 'Addon file extension should be vsix ',
        icon: 'error',
        confirmButtonText: 'Ok',
        background: '#242a2f',
      });
    }
    setAddonRawData(event.target.files[0]);
  };
  const updateAddonLogo = (event:any) => {
    setAddonLogo(event.target.files[0]);
  };
  const update = (prop:any) => (event:any) => {
    try {
      props.handleEditing(event, prop);
    } catch (e) {
      console.log(e);
    }
    if (prop === 'originLink') {
      if (event.target.value.includes('https://github.com/')) {
      };
    };
    if (prop == 'name') {
      event.target.value = event.target.value.replace(/[^\w\s]/gi, '');
    };
    setAddonData({
      ...addonData,
      [prop]: event.target.value,
    });
  };
  const updateBtn = () => {
    setUploadingBtn(!uploadingBtn);
  };
  const uploadAddonToFirebase = async (e:any) => {
    e.preventDefault();
    updateBtn();
    if (addonData.fileName === '' && adddonRawData.name !== '' &&
    addonData.logoName === '' && addonLogoFile.name !== '') {
      const errorMsg = await addonCheck(addonData, tags, adddonRawData, addonLogoFile, oldAddonName);
      if (!userData) {
        Swal.fire({
          title: 'Error',
          text: 'Please login First',
          icon: 'error',
          confirmButtonText: 'Ok',
          background: '#242a2f',
        });
        setTimeout(() => navigate('/login'));
      }
      if (!errorMsg) {
        // setUploadingBtn(!uploadingBtn);
        const apiURL = await createApiAndGetRepo(addonData.originLink);
        const data = await uploadToFirebase(adddonRawData);
        const addonLogoUrl = await uploadAddonLogo(addonLogoFile);
        const lastCommit = await getLastCommit(apiURL);
        const addonDataUpdated = {
          ...addonData,
          'downloadLink': data.downloadUrl,
          'fileName': data.fileName,
          'addonLogo': addonLogoUrl.addonLogoUrl,
          'logoName': addonLogoUrl.logoName,
          'apiLink': apiURL,
          'tags': [...tags],
          'creator': userData.username,
          'featured': false,
          'uploadDate': new Date().toString(),
          'lastCommit': lastCommit,
        };
        const uploaded = await uploadAddon(addonDataUpdated);
        if (uploaded) {
          toast.fire({
            icon: 'success',
            title: 'Uploaded successfully',
          });
          createNotification('admin', `${userData.username}|created addon|${addonData.name}`, 'An');
          navigate('/');
        }
      } else {
        updateBtn();
        Swal.fire({
          title: 'Error',
          text: errorMsg,
          icon: 'error',
          confirmButtonText: 'Ok',
          background: '#242a2f',
        });
      }
    } else if (addonData.fileName !== '' && adddonRawData.name === '' &&
    addonData.logoName !== '' && addonLogoFile.name === '') {
      const errMsg = addonCheck2(addonData, tags);
      if (!errMsg) {
        const apiURL = await createApiAndGetRepo(addonData.originLink);
        const addonDataUpdated = {
          ...addonData,
          'apiLink': apiURL,
          'tags': [...tags],
        };
        await updateAddon(addonDataUpdated, oldAddonName);
        createNotification('admin', `${userData.username}|updated addon|${addonData.name}`, 'An');
        navigate('/');
      } else {
        setUploadingBtn(false);
        Swal.fire({
          title: 'Error',
          text: errMsg,
          icon: 'error',
          confirmButtonText: 'Ok',
          background: '#242a2f',
        });
      }
    } else {
      const errorMsg = await addonCheck(addonData, tags, adddonRawData, addonLogoFile, oldAddonName);
      if (!userData) {
        Swal.fire({
          title: 'Error',
          text: 'Please login First',
          icon: 'error',
          confirmButtonText: 'Ok',
          background: '#242a2f',
        });
        setTimeout(() => navigate('/login'), 1000);
      }
      if (!errorMsg) {
        // setUploadingBtn(!uploadingBtn);
        const apiURL = await createApiAndGetRepo(addonData.originLink);
        const data = await uploadToFirebase(adddonRawData);
        const addonLogoUrl = await uploadAddonLogo(addonLogoFile);
        const lastCommit = await getLastCommit(apiURL);
        const addonDataUpdated = {
          ...addonData,
          'downloadLink': data.downloadUrl,
          'fileName': data.fileName,
          'addonLogo': addonLogoUrl.addonLogoUrl,
          'logoName': addonLogoUrl.logoName,
          'apiLink': apiURL,
          'tags': [...tags],
          'creator': userData.username,
          'featured': false,
          'uploadDate': new Date().toString(),
          'lastCommit': lastCommit,
        };
        await updateAddon(addonDataUpdated, oldAddonName);
        createNotification('admin', `${userData.username}|updated addon|${addonData.name}`, 'An');
        setTimeout(() => navigate('/'), 1000);
      } else {
        setUploadingBtn(false);
        Swal.fire({
          title: 'Error',
          text: errorMsg,
          icon: 'error',
          confirmButtonText: 'Ok',
          background: '#242a2f',
        });
      }
    }
    // setUploadingBtn(!uploadingBtn);
  };
  const updateTags = (e:any) => {
    e.target.value = e.target.value.replace(/[^\w\s]/gi, '');
    setTag(e.target.value);
  };
  const createTag = (e:any) => {
    e.preventDefault();
    const allTags = [...tags];
    if (allTags[0] === '') {
      allTags[0] = tag.trim();
    } else {
      if (allTags.includes(tag.trim())) {
        setTag('');
      } else {
        allTags.push(tag.trim());
      }
    }
    setTag('');
    setTags(allTags);
    props.handleEditing(e, tag);
    setAddonData({
      ...addonData,
      ['tags']: [...allTags],
    });
  };
  const handleDeleteTag = (e:any, tag:string) => {
    const filteredTags = tags.filter((el) => el !== tag);
    setTags(filteredTags);
    props.handleEditing(e, tag);
    setAddonData({
      ...addonData,
      ['tags']: [...filteredTags],
    });
  };
  if (!addonData && !userData) {
    return (<h1>Loading</h1>);
  } else {
    return (
      <div className='container mx-auto max-w-5xl mb-10 bg-mainBG pb-10 mt-10'>
        <label className='block text-textW text-sm font-bold mb-2' htmlFor='name'>Name of the Addon<span className='text-red-400'> *</span></label>
        <input type='text' pattern='[a-zA-Z]' onChange={update('name')} value={addonData.name} className='my-2 shadow appearance-none border-filler border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none text-textW bg-navbar' name='name' id='name' required maxLength={30} minLength={3}></input>
        <label className='block text-textW text-sm font-bold mb-2' htmlFor='description'>Short Description of the Addon<span className='text-red-400'> *</span></label>
        <textarea id='message' rows={4} className='my-2 block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500' placeholder='Short description of the addon' required onChange={update('description')} value={addonData.description} maxLength={200} minLength={20}></textarea>
        {addonData.originLink.includes('https://github.com/') ? <div>
          <label className='block text-textW text-sm font-bold mb-2' htmlFor='originLink'>GitHub repository<span className='text-red-400'> *</span></label>
          <input type='text' onChange={update('originLink')} value={addonData.originLink} className='my-2 shadow appearance-none border  border-filler rounded w-full py-2 px-3 text-textW leading-tight focus:outline-none focus:shadow-outline bg-navbar' id='originLink' name='originLink' required></input></div> : <div>
            <label className='block text-textW text-sm font-bold mb-2' htmlFor='originLink'>GitHub repository<span className='text-red-400'> *</span><span className='text-gray-400 font-normal italic'> Must match https://github.com/</span></label>
            <input type='text' onChange={update('originLink')} value={addonData.originLink} className='my-2 shadow appearance-none border-filler border rounded w-full py-2 px-3 text-textW leading-tight focus:outline-none focus:shadow-outline bg-navbar' id='originLink' name='originLink' required></input></div>}
        {/* <input type='text' onChange={update('originLink')} value={addonData.originLink} className='my-2 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline' id='originLink' name='originLink' required></input> */}
        <form onSubmit={(e) => createTag(e)}>
          {tags ? tags.map((tag, key) => tag !== '' ? <div key={key} id='addon-tag' onClick={((e) => handleDeleteTag(e, tag))} className='inline-block bg-filler border border-filler rounded-full px-3 py-1 text-sm font-semibold text-textW cursor-pointer mr-2 mb-2 hover:bg-sGreen hover:text-mainBG'>{tag}</div> : <></>) : <></>}
          <label className='block text-textW text-sm font-bold mb-2' htmlFor='tag'>New Tag<span className='text-red-400'> *</span><span className='text-gray-400 font-normal italic'> Must include atleast 1 tag</span></label>
          <input type='text' onChange={updateTags} value={tag} className='my-2 shadow appearance-none border-filler border rounded w-full py-2 px-3 text-textW bg-navbar leading-tight focus:outline-none focus:shadow-outline' name='tag' id='tag' required maxLength={20} minLength={2}></input>
        </form>
        <label className='block text-textW text-sm font-bold mb-2' htmlFor='file_input'>Addon File<span className='text-red-400'> *</span></label>
        <div className='my-2 flex justify-center items-center w-full'>
          <label htmlFor='uploadFile' className='flex flex-col justify-center items-center w-full h-18 bg-gray-50 rounded-lg border-2 border-gray-300 border-dashed cursor-pointer dark:hover:bg-bray-800 dark:bg-gray-700 hover:bg-gray-100 dark:border-gray-600 dark:hover:border-gray-500 dark:hover:bg-gray-600'>
            <div className='my-2 flex flex-col justify-center items-center pt-5 pb-6'>
              <svg className='mb-3 w-10 h-10 text-gray-400' fill='none' stroke='currentColor' viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg'><path strokeLinecap='round' strokeLinejoin='round' strokeWidth='2' d='M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12'></path></svg>
              <p className='mb-2 text-sm text-gray-500 dark:text-gray-400'><span className='font-semibold'>{adddonRawData.name !== '' ? `${adddonRawData.name}`:'Click to upload or drag and drop'}</span></p>
            </div>
            <input id='uploadFile' type='file' className='hidden' onChange={updateUploadFile}/>
          </label>
        </div>
        <label className='block text-textW text-sm font-bold mb-2' htmlFor='file_input'>Addon Logo<span className='text-red-400'> *</span></label>
        <div className='flex justify-center items-center w-full'>
          <label htmlFor='uploadImg' className='flex flex-col justify-center items-center w-full h-18 bg-gray-50 rounded-lg border-2 border-gray-300 border-dashed cursor-pointer dark:hover:bg-bray-800 dark:bg-gray-700 hover:bg-gray-100 dark:border-gray-600 dark:hover:border-gray-500 dark:hover:bg-gray-600'>
            <div className='flex flex-col justify-center items-center pt-5 pb-6'>
              <svg className='mb-3 w-10 h-10 text-gray-400' fill='none' stroke='currentColor' viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg'><path strokeLinecap='round' strokeLinejoin='round' strokeWidth='2' d='M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12'></path></svg>
              <p className='mb-2 text-sm text-gray-500 dark:text-gray-400'><span className='font-semibold'>{addonLogoFile.name !== '' ? addonLogoFile.name : 'Click to upload'}</span> or drag and drop</p>
              <p className="mt-1 text-sm text-gray-500 dark:text-gray-300" id="file_input_help">SVG, PNG, JPG or GIF.</p>
            </div>
            <input id='uploadImg' type='file' className='hidden' onChange={updateAddonLogo} accept='image/*'/>
          </label>
        </div>
        {!uploadingBtn ? <button type='submit' className='justify-self-center border-2 border-sGreen my-2 text-sGreen font-bold bg-mainBG hover:bg-sGreen focus:ring-4 focus:outline-none focus:ring-sGreen font-bold rounded-lg text-sm px-5 py-2.5 text-center dark:bg-mainBG dark:hover:bg-sGreen hover:text-mainBG dark:focus:ring-sGreen alg' onClick={uploadAddonToFirebase}>{JSON.stringify(props.addon) === JSON.stringify(addonData)? 'Update Addon' : 'Upload Addon'}</button> :
                            <button className='justify-self-center text-mainBG bg-sGreen hover:bg-sGreen focus:ring-4 focus:outline-none focus:ring-sGreen font-medium rounded-lg text-sm pr-5 py-2.5 my-2 text-center dark:bg-sGreen dark:hover:bg-sGreen dark:focus:ring-sGreen alg'><div className="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>{JSON.stringify(props.addon) === JSON.stringify(addonData) ? 'Updating...' : 'Uploading...'}</button>}
      </div>
    );
  };
};
export default Upload;
