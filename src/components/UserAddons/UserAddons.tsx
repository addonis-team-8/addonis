/* eslint-disable no-unused-vars */
import {useEffect, useState} from 'react';
import {getUserAddons} from '../../services/users.service';
import AddonCard2 from '../AddonCard2';
import Pagination from '../Pagination';
import {Link} from 'react-router-dom';
const UserAddons = (user:any) => {
  const [addons, setAddons] = useState<any>();
  const [currentPage, setCurrentPage] = useState(1);
  const [addonsPerPage] = useState(8);
  const getData = async () => {
    const res = await getUserAddons(user.username);
    console.log(res);
    setAddons(res);
  };
  useEffect(() => {
    getData();
  }, []);
  const indexOfLastAddon = currentPage * addonsPerPage;
  const indexOfFirstAddon = indexOfLastAddon - addonsPerPage;
  let currentAddons = {};
  if (addons && Object.keys(addons).length !== 0) {
    currentAddons = Object.fromEntries(
        Object.entries(addons).slice(indexOfFirstAddon, indexOfLastAddon),
    );
  }
  const paginate = (pageNumber:any) => setCurrentPage(pageNumber);

  return (
    <div className=''>
      <div className='mx-auto my-5 grid grid-cols-1 br1:grid-cols-2 br2:grid-cols-3 br3:grid-cols-4 br4:grid-cols-5 gap-2'>
        {addons ? Object.keys(currentAddons).map((addonKey, key) => {
          const keyTyped = addonKey as keyof typeof addons;
          const addon = addons[keyTyped];
          return (
            <AddonCard2 key={key} {...addon}></AddonCard2>
          );
        }) : <></>}
      </div>
      <div className='flex border border-fillerD justify-center '>
        {addons ? <Pagination addonsPerPage={addonsPerPage} totalAddons={Object.keys(addons).length} paginate={paginate} currentPage={currentPage}></Pagination> : <div className='border w-full'>
          <h1>No Addons found ! You can always <Link to='/upload'><span className='text-sGreen'>Upload</span></Link> one.</h1>
        </div> }
      </div>
    </div>
  );
};

export default UserAddons;
