import {useContext, useEffect, useState} from 'react';
import {AppContext} from '../../providers/AppContext';
import {useLocation} from 'react-router-dom';
// import {updateUser} from '../../services/users.service';
import Edit from '../Edit/Edit';
import UserAddons from '../UserAddons/UserAddons';
import {logoutUser} from '../../services/auth.service';
import Main from '../Main/Main';
import {useNavigate} from 'react-router-dom';

const UserDetailed = (props:any) => {
  const userObj = {
    username: '',
    email: '',
    logoUrl: '',
    logoName: '',
    phoneNumber: '',
  };
  const {userData, setContext} = useContext(AppContext);
  const [user, setUser] = useState(userObj);
  const location = useLocation();
  const [position, setPosition] = useState('dashboard');
  const navigate = useNavigate();
  const handlePosition = (e:string) => {
    setPosition(e);
  };
  const {state} = location as any;
  if (user) {
    if (user.email === '' && userData) {
      setUser({...userData});
    }
  }
  useEffect(() => {
    if (state) {
      setUser(state.user);
    } else {
      setUser(userData);
    }
  }, []);
  const handleLogout = async () => {
    await logoutUser();
    setContext({user: undefined, userData: null});
    navigate('/');
  };
  return (
    <Main title={'Profile'}>
      <div className='w-11/12 mx-auto text-textW flex border border-fillerD my-2 px-4'>
        <div className="border border-navbar w-1/6 h-fit text-textW px-2">
          <button onClick={() => handlePosition('edit')} value='edit' className='w-full text-left my-0.5 py-1 focus:bg-navbar font-semibold focus:text-sBlue'>Edit</button>
          <button value='addons' onClick={() => handlePosition('addons')} className='w-full text-left my-0.5 py-1 focus:bg-navbar font-semibold focus:text-sGreen'>Addons</button>
          <button value='logout' onClick={handleLogout} className='w-full text-left my-0.5 py-1 focus:bg-navbar font-semibold focus:text-sGreen'>Logout</button>
        </div>
        {position === 'edit' ? <Edit/> : <></>}
        <div className='w-6/6 h-auto px-4'>
          {position === 'addons' ? <UserAddons {...user}/> : <></>}
        </div>
      </div>
    </Main>
  );
};
export default UserDetailed;
