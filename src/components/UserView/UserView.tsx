import {useEffect, useState, useContext} from 'react';
import {useLocation} from 'react-router-dom';
import {blockUser, getUserAddons, getUserByUsername} from '../../services/users.service';
import AddonCard2 from '../AddonCard2';
import {AppContext} from '../../providers/AppContext';
import toast from '../Toast/toaster';
import {createNotification} from '../../services/notification.service';
import Main from '../Main/Main';
import Pagination from '../Pagination';
type userObJ = {
  username: string,
  dateRegistered: string,
  email: string,
  phoneNumber:string,
  type:string,
  uid:string,
  logoName:string,
  logoUrl:string,
}
const UserView = () => {
  const [user, setUser] = useState<userObJ>();
  const [userAddons, setUserAddons] = useState<any>();
  const [currentPage, setCurrentPage] = useState(1);
  const [addonsPerPage] = useState(10);
  const {userData} = useContext(AppContext);
  const location = useLocation();
  const {state} = location as any;
  useEffect(() => {
    (async () => {
      const userData = (await getUserByUsername(state.creator)).val();
      userData.dateRegistered = new Date(userData.dateRegistered).toLocaleDateString();
      const userDataAddons = await getUserAddons(state.creator);
      setUserAddons(userDataAddons);
      setUser(userData);
    })();
  }, []);
  const handleBlock = () => {
    let msg = '';
    let setter = '';
    if (user?.type === 'user') {
      msg = 'User blocked';
      setter = 'blocked';
    } else {
      msg = 'User unblocked';
      setter = 'user';
    }
    toast.fire({
      icon: 'success',
      title: msg,
    });
    if (user) {
      blockUser(state.creator, user?.type);
      createNotification(state.creator, `${userData.username}|has blocked you`, 'Un');
      setUser({
        ...user,
        ['type']: setter,
      });
    };
  };

  const indexOfLastAddon = currentPage * addonsPerPage;
  const indexOfFirstAddon = indexOfLastAddon - addonsPerPage;
  let currentAddons;
  if (userAddons && Object.keys(userAddons).length !== 0) {
    // eslint-disable-next-line no-unused-vars
    currentAddons = Object.fromEntries(
        Object.entries(userAddons).slice(indexOfFirstAddon, indexOfLastAddon),
    );
  }
  const paginate = (pageNumber:any) => setCurrentPage(pageNumber);
  return (
    <Main title='User Panel'>
      <div className="text-textW">
        {user ? <div className='border border-filler w-11/12 mx-auto my-4'>
          <div className='flex'>
            <div className='w-fit'>
              <img src={user.logoUrl} className='w-36 p-4'></img>
            </div>
            <div className='text-center'>
              <h1 className='font-semibold'><span className={user.type === 'user' ? 'text-sGreen' : user.type === 'admin' ? 'text-sBlue' : 'text-sRed'}>{user.username}</span><span className='font-normal text-gray-400 text-sm'> member since <span className='text-textW'> {user.dateRegistered}</span></span></h1>
              <h1>Uploaded Addons: <span className='text-sGreen font-semibold'>{userAddons ? Object.keys(userAddons).length : 0}</span></h1>
              {userData && userData.type === 'admin' ? <div className='text-textW'>
                <button onClick={handleBlock} className={user.type === 'blocked' ? 'text-sGreen border-2 border-sGreen rounded-lg hover:bg-dGreen hover:text-mainBG font-semibold px-5 py-1': 'text-sRed border-2 border-sRed rounded-lg hover:bg-sRed hover:text-mainBG font-semibold px-5 py-1'}>{user.type === 'blocked' ? 'Unblock User' : 'Block User'}</button>
              </div> : <></>}
            </div>
          </div>
          <div className=''>
            <div className='mx-auto my-5 grid grid-cols-1 br1:grid-cols-3 br2:grid-cols-4 br3:grid-cols-5 br4:grid-cols-5 gap-2'>
              {userAddons ? Object.keys(userAddons).map((addonKey, key) => {
                const keyTyped = addonKey as keyof typeof userAddons;
                const addon = userAddons[keyTyped];
                return (
                  <div key={key} className='border border-filler py-2'>
                    <AddonCard2 key={key} {...addon}></AddonCard2>
                  </div>
                );
              }) : <></>}
            </div>
          </div>
        </div> : <></> }
        <div className='flex border border-fillerD justify-center '>
          {userAddons ? <Pagination addonsPerPage={addonsPerPage} totalAddons={Object.keys(userAddons).length} paginate={paginate} currentPage={currentPage}></Pagination> : <></> }
        </div>
      </div>
    </Main>
  );
};
export default UserView;
