import {useEffect, useState} from 'react';
import {getAllUsers} from '../../services/users.service';
import {Link} from 'react-router-dom';
import Pagination from '../Pagination';
const Users = () => {
  const [users, setUsers] = useState<any[]>([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [addonsPerPage] = useState(8);
  const getData = async () => {
    const userData = await getAllUsers('users');
    setUsers(userData);
  };
  useEffect(() => {
    getData();
  }, []);
  const indexOfLastAddon = currentPage * addonsPerPage;
  const indexOfFirstAddon = indexOfLastAddon - addonsPerPage;
  const currentUsers = Object.fromEntries(
      Object.entries(users).slice(indexOfFirstAddon, indexOfLastAddon),
  );
  const paginate = (pageNumber:any) => setCurrentPage(pageNumber);
  return (
    <div className="text-textW border border-navbar flex-wrap w-full px-2">
      <div className='mx-auto my-5 grid grid-cols-1 br1:grid-cols-2 br2:grid-cols-3 br3:grid-cols-4 br4:grid-cols-5 gap-2'>
        {currentUsers ? Object.keys(currentUsers).map((userKey, key) => {
          const keyTyped = userKey as keyof typeof users;
          const user = users[keyTyped];
          return (
            <Link key={key} to={`/user/${user.username}`} state={{creator: user.username}}>
              <div className='border border-filler h-80'>
                <div className='h-4/6 my-2 px-2'>
                  <img src={user.logoUrl} className='object-fit mx-auto h-auto w-4/6 my-2'></img>
                </div>
                <div className='text-center'>
                  <h1 className='font-semibold'><span>{user.type}: </span>{user.username}</h1>
                </div>
              </div>
            </Link>
          );
        }) : <></>}
      </div>
      <div className='flex border border-fillerD justify-center '>
        <Pagination addonsPerPage={addonsPerPage} totalAddons={Object.keys(users).length} paginate={paginate} currentPage={currentPage}></Pagination>
      </div>
    </div>
  );
};

export default Users;
