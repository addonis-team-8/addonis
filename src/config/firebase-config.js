import {initializeApp} from 'firebase/app';
import {getAuth} from 'firebase/auth';
import {getDatabase} from 'firebase/database';
import {getStorage} from 'firebase/storage';

const firebaseConfig = {
  apiKey: 'AIzaSyBfvZly-CATlqwFgA8yvFuJ3RBhCEpgq0Y',
  authDomain: 'addonis-c7df5.firebaseapp.com',
  databaseURL: 'https://addonis-c7df5-default-rtdb.europe-west1.firebasedatabase.app',
  projectId: 'addonis-c7df5',
  storageBucket: 'addonis-c7df5.appspot.com',
  messagingSenderId: '407291893508',
  appId: '1:407291893508:web:c90b3ef8340f654e486251',
};

export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getDatabase(app);
export const storage = getStorage(app);
