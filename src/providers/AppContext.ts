import {User} from '@firebase/auth';
import {createContext, Dispatch, SetStateAction} from 'react';

export const AppContext = createContext<{user: User | undefined, userData: any, setContext:Dispatch<SetStateAction<{ user: User | undefined; userData: any; }>>}>({
  user: undefined,
  userData: undefined,
  setContext: () => {},
});
