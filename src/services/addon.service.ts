import axios from 'axios';
import {db, storage} from '../config/firebase-config';
import {set, ref, get, query} from 'firebase/database';
import {deleteObject, getDownloadURL, ref as storageRef, uploadBytes} from 'firebase/storage';
import {v4 as uuidv4} from 'uuid';
import {updateUserAddons} from './users.service';
const gitHubAuthToken = `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`;
const header = {
  'Authorization': `Token ${gitHubAuthToken}`,
};

interface addonObj {
  name: string,
  description: string,
  creator: string,
  tags: string[],
  downloads: number,
  ratings: number,
  downloadLink: string,
  originLink: string,
  apiLink: string,
  issues: string,
  pulls: string,
  lastCommit: string,
  uploadDate: string,
  approved: boolean,
  addonLogo: string,
  fileName: string,
  logoName: string,
  ide: string,
  featured: boolean,
};

export const createApiAndGetRepo = async (repo:string) => {
  const apiURL = 'https://api.github.com/repos/' + repo.slice(19);
  let data = null;
  try {
    data = await axios.get(apiURL, {
      headers: header,
    });
  } catch (error) {
  }
  if (data) {
    return apiURL;
  } else {
    return null;
  }
};
export const getRepo = async (repo: string) => {
  const {data} = await axios.get(repo, {
    headers: header,
  });
  return data;
};
export const getGitHubData = async (repo: string) => {
  const {data} = await axios.get(repo, {
    headers: header,
  });
  const pulls = await axios.get(repo+'/pulls', {
    headers: header,
  });
  const lastCommit = await axios.get(repo+'/commits', {
    headers: header,
  });
  const dateOfCommit = new Date(lastCommit.data[0].commit.author.date);
  const timeNow = Date.now();
  const daysSinceLastCommit = Math.ceil((timeNow - dateOfCommit.getTime()) / (1000 * 3600 * 24));
  const numberOfPulls = pulls.data.length;
  const issuesCount = data.open_issues_count;
  return {issuesCount, numberOfPulls, daysSinceLastCommit};
};
function b64DecodeUnicode(str: string) {
  // Going backwards: from bytestream, to percent-encoding, to original string.
  return decodeURIComponent(atob(str).split('').map(function(c) {
    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));
}
export const getRepoMd = async (repo:any) => {
  const apiReqLink = repo + '/readme';
  const {data} = await axios.get(apiReqLink, {
    headers: header,
  });
  let content = data.content;
  content = content.replace(/(\r\n|\n|\r)/gm, '');
  const decodedContent = b64DecodeUnicode(content);
  return decodedContent;
};
export const getLastCommit = async (apiLink:string | null) => {
  if (apiLink) {
    const data = await axios.get(apiLink+'/commits', {
      headers: header,
    });
    return (data.data[0].commit.author.date);
  }
};

const updateTagAddons = async (tag:string, addonName:string) => {
  const reference = ref(db, `tags/${tag}`);
  const tagData = await (await get(reference)).val();
  tagData[addonName] = true;
  await set(reference, tagData);
};

export const createTagBucket = async (tag:string, addonName:string) => {
  const reference = ref(db, `tags/${tag}`);
  const tagData = await (await get(reference)).val();
  if (!tagData) {
    const firstEntry = <any>{};
    firstEntry[addonName] = true;
    await set(reference, firstEntry);
  } else {
    updateTagAddons(tag, addonName);
  };
};
export const uploadAddon = async (addon:any) => {
  const firstLetter = addon.name[0].toUpperCase();
  addon.name = firstLetter + addon.name.slice(1);
  const reference = ref(db, `pending/${addon.name}`);
  addon['ratings'] = 0;
  await set(reference, {...addon});
  updateUserAddons(addon.creator, addon.name);
  return true;
};
export const deleteAddonFromTag = async (tag:string, addonName:string) => {
  const reference = ref(db, `tags/${tag}/${addonName}`);
  await set(reference, null);
};

export const uploadToFirebase = async (file:any) => {
  const reference = storageRef(storage, `addons/${file.name}`);
  await uploadBytes(reference, file);
  const downloadUrl = await getDownloadURL(storageRef(storage, `addons/${file.name}`));
  return {downloadUrl, fileName: file.name};
};

export const uploadAddonLogo = async (file:any) => {
  const uniqueID = uuidv4();
  const reference = storageRef(storage, `addonImages/${uniqueID}`);
  await uploadBytes(reference, file);
  const addonLogoUrl = await getDownloadURL(storageRef(storage, `addonImages/${uniqueID}`));
  return {addonLogoUrl, logoName: uniqueID};
};

export const deleteAddonFromStorage = async (addon:any) => {
  let location = `pending/${addon.name}`;
  if (addon.approved) {
    location =
     `addons/${addon.name}`;
  }
  const fileRef = storageRef(storage, `addons/${addon.fileName}`);
  const logoRef = storageRef(storage, `addonImages/${addon.logoName}`);
  const addonRef = ref(db, location);
  await addon.tags.map(async (tag:string) => {
    await deleteAddonFromTag(tag, addon.name);
  });
  const userRef = ref(db, `users/${addon.creator}/addons/${addon.name}`);
  await set(userRef, null);
  await set(addonRef, null);
  await deleteObject(fileRef);
  await deleteObject(logoRef);
};
export const getAddon = async (addonName:string) => {
  const addonRef = ref(db, `addons/${addonName}`);
  const data = await (await get(addonRef)).val();
  if (data) {
    return data;
  } else {
    const reference = ref(db, `featured/${addonName}`);
    const data = await (await get(reference)).val();
    if (data) {
      return data;
    } else {
      const reference = ref(db, `pending/${addonName}`);
      const data = await (await get(reference)).val();
      if (data) {
        return data;
      } else {
        return null;
      }
    }
  }
};

export const getAllAddons = async (type:string = 'addons') => {
  const addons = await get(query(ref(db, type)));
  const addonsObject = addons.val();
  return addonsObject;
};

export const getAddonsByTag =async (tag:string) => {
  if (tag === '') {
    return null;
  }
  const data = await (await get(ref(db, `tags/${tag}`))).val();
  const addonObj = <any>{};
  if (data) {
    await Object.keys(data).reduce(async (acc, addon) => {
      await acc;
      const addonData = await getAddon(addon);
      addonObj[addon] = addonData;
    }, Promise.resolve());
    return addonObj;
  } else {
    return null;
  }
};

export const filterAddonsByTag:any = (addonObj:any, tags:string[]) => {
  if (tags.length === 0 && addonObj) {
    return addonObj;
  }
  const tag = tags[0];
  tags = tags.slice(1);
  const filtredObj = <any>{};
  if (addonObj) {
    Object.keys(addonObj).map((addon) => {
      if (addonObj[addon].tags.includes(tag)) {
        filtredObj[addon] = addonObj[addon];
      }
    });
  }
  return filterAddonsByTag(filtredObj, tags);
};

export const getAddonsByTags = async (tags:string[]) => {
  const addons = await getAddonsByTag(tags[0]);
  tags = tags.slice(1);
  const filteredAddons = filterAddonsByTag(addons, tags);
  return filteredAddons;
};

// getAddonsByTags(['tag1','tag2','tag 10','tag 15'])

export const addonCheck = async (addon:addonObj, tags:string[], addonRawData:any, addonLogoFile:any, oldAddonName:string) => {
  if (addon.name.length < 3 || addon.name.length > 30) {
    return 'Name must be between 3 and 30 characters';
  };
  if (addon.description.length < 20 || addon.description.length > 200) {
    return 'Addon description must be between 20 and 200 characters';
  };
  if (!addon.originLink.includes('https://github.com/')) {
    return 'GitHub link is not correct';
  };
  if (tags.length < 1 || tags[0] === '') {
    return 'There must be atleast one tag';
  };
  if (addonRawData.name === '') {
    return 'Add the Addon File';
  }
  if (addonLogoFile.name === '') {
    return 'Include a Logo for the Addon';
  };
  if (oldAddonName !== addon.name) {
    const featuredRef = (await get(ref(db, `featured/${addon.name}`))).val();
    const addonRef =(await get(ref(db, `addons/${addon.name}`))).val();
    const pendingRef = await (await get(ref(db, `pending/${addon.name}`))).val();
    if (featuredRef || addonRef || pendingRef) {
      return 'Addon Name is already taken !';
    }
  }
};
export const addonCheck2 = (addon:any, tags:string[]) => {
  if (addon.name.length < 3 || addon.name.length > 30) {
    return 'Name must be between 3 and 30 characters';
  };
  if (addon.description.length < 20 || addon.description.length > 200) {
    return 'Addon description must be between 20 and 200 characters';
  };
  if (!addon.originLink.includes('https://github.com/')) {
    return 'GitHub link is not correct';
  };
  if (tags.length < 1 || tags[0] === '') {
    return 'There must be atleast one tag';
  };
};
export const approveAddon = (addon:any) => {
  const reference = ref(db, `addons/${addon.name}`);
  const removeRef = ref(db, `pending/${addon.name}`);
  addon.approved = true;
  addon.tags.map(async (tag:string) => {
    await createTagBucket(tag, addon.name);
  });
  set(removeRef, null);
  set(reference, addon);
};

export const featureAddon = async (addon:any) => {
  if (!addon.featured) {
    addon.featured = true;
    const reference = ref(db, `featured/${addon.name}`);
    const removeRef = ref(db, `addons/${addon.name}`);
    await set(removeRef, null);
    await set(reference, addon);
  } else if (addon.featured) {
    addon.featured = false;
    const reference = ref(db, `addons/${addon.name}`);
    const removeRef = ref(db, `featured/${addon.name}`);
    await set(removeRef, null);
    await set(reference, addon);
  }
};

export const addDownload = async (name:string, featured:boolean) => {
  let path = 'addons/';
  if (featured) {
    path = 'featured/';
  }
  const reference = ref(db, `${path}${name}/downloads`);
  const downloads = await (await get(reference)).val();
  const newDownloads = downloads + 1;
  set(reference, newDownloads);
};

export const updateAddon = async (addon:any, oldAddonName:string) => {
  let where = '';
  const oldAddon = await getAddon(oldAddonName);
  if (addon.fileName !== oldAddon.fileName) {
    await deleteAddonFromStorage(oldAddon);
  }
  if (oldAddon.approved) {
    if (oldAddon.featured) {
      where = 'featured/';
    } else {
      where = 'addons/';
    }
  } else {
    where = 'pending/';
  }
  await set(ref(db, `${where}${oldAddonName}`), null);
  const reference = ref(db, `${where}${addon.name}`);
  await set(reference, addon);
  await oldAddon.tags.map(async (tag:string) => {
    await deleteAddonFromTag(tag, oldAddonName);
  });
  await addon.tags.map(async (tag:string) => {
    await createTagBucket(tag, addon.name);
  });
  const userAddonRef = ref(db, `users/${addon.creator}/addons/${oldAddonName}`);
  const userNewAddonRef = ref(db, `users/${addon.creator}/addons/${addon.name}`);
  set(userAddonRef, null);
  set(userNewAddonRef, addon.name);
  return 'Update Successful';
};

export const rateAddon = async (addonName:string, user:string, rating:number, featured:boolean) => {
  console.log('is fe', featured);
  let path = 'addons/';
  if (featured) {
    path = 'featured/';
  }
  const reference = ref(db, `${path}${addonName}/ratings`);
  const data = await (await get(reference)).val();
  if (data) {
    data[user] = rating;
    await set(reference, data);
  } else {
    const ratings = <any>{};
    ratings[user] = rating;
    await set(ref(db, `${[path]}${addonName}/ratings`), ratings);
  }
};
