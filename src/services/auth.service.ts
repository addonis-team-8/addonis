import {createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut, updateEmail} from 'firebase/auth';
import {auth} from '../config/firebase-config';

export const registerUser = (email = '', password = '') => {
  return createUserWithEmailAndPassword(auth, email, password);
};

export const loginUser =async (email = '', password = '') => {
  return signInWithEmailAndPassword(auth, email, password);
};


export const logoutUser = () => {
  return signOut(auth);
};

export const updateUserEmail = async (auth2:any, email:string) => {
  if (auth2 === '') {
    auth2 = auth;
  }
  try {
    await updateEmail(auth2.currentUser, email);
    return true;
  } catch (e) {
    return false;
  }
};
