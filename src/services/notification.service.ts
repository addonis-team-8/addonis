import {db} from '../config/firebase-config';
import {set, ref, get} from 'firebase/database';
import {v4 as uuidv4} from 'uuid';
export const createNotification = async (user:string, notification:string, path:string) => {
  const id = uuidv4();
  const saveRef = ref(db, `users/${user}/${path}/${id}`);
  notification = 1 + notification+'|'+id;
  await set(saveRef, notification);
};
// userNotifications('burger', 'Addon - notification', 'An');

export const clearNotification = async (user:string, notification:string, path:string) => {
  const reference = ref(db, `users/${user}/${path}`);
  const data = await (await get(reference)).val();
  const keys = Object.keys(data).map((v) => {
    if (data[v] === notification) {
      return v;
    }
  });
  keys.map((key) => {
    set(ref(db, `users/${user}/${path}/${key}`), null);
  });
};
export const setSeenNotifications = async (user:string, notifications:any[string], path:string) => {
  notifications.map( async (not:any) => {
    if (not[0] === '1') {
      const id = not.slice(not.lastIndexOf('|')+1);
      not = 0+not.slice(1);
      await set(ref(db, `users/${user}/${path}/${id}`), not);
    }
  });
};

export const clearAllNotifications = (user:string, path:string) => {
  set(ref(db, `users/${user}/${path}`), null);
};
// clearNotification('burger', 'notification', 'Un')
