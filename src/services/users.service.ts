/* eslint-disable no-unused-vars */
import {db, storage} from '../config/firebase-config';
import {get, set, ref, query, equalTo, orderByChild, update} from 'firebase/database';
import {deleteObject, getDownloadURL, ref as storageRef, uploadBytes} from 'firebase/storage';
import {v4 as uuidv4} from 'uuid';
import {getAddon} from './addon.service';


export const createUser = (username = '', uid = '', email = '', phoneNumber = '', logoUrl:string, logoName:string) => {
  return set(ref(db, `users/${username}`), {
    username,
    uid,
    email,
    phoneNumber,
    dateRegistered: new Date().toString(),
    type: 'user',
    logoUrl: 'https://www.pngarts.com/files/10/Default-Profile-Picture-Download-PNG-Image.png',
    logoName,
    addons: false,
  });
};

export const getUserByUsername = async (username = '') => {
  return get(ref(db, `users/${username}`));
  // const userData = await (await get(ref(db, `users/${username}`))).val();
  // return userData;
};

export const getUserData = async (uid = '') => {
  const userData = await get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)));
  return userData;
};

export const updateAvatar = (username = '', url = '') => {
  return update(ref(db), {
    [`users/${username}/avatarUrl`]: url,
  });
};

export const uploadLogo = async (file:any) => {
  const uniqueID = uuidv4();
  const reference = storageRef(storage, `userImages/${uniqueID}`);
  await uploadBytes(reference, file);
  const userLogoUrl = await getDownloadURL(storageRef(storage, `userImages/${uniqueID}`));
  return {userLogoUrl, userLogoName: uniqueID};
};

export const getAllUsers = async (type:string = 'users') => {
  const users = await get(query(ref(db, type)));
  const addonsObject = users.val();
  return addonsObject;
};

export const updateUserAddons = async (creator:string, addonName:string) => {
  const reference = ref(db, `users/${creator}/addons`);
  let userData = await (await get(reference)).val();
  if (!userData) {
    userData = {};
    userData[addonName] = addonName;
  } else {
    userData[addonName] = addonName;
  }
  const updateReference = ref(db, `users/${creator}/addons`);
  set(updateReference, userData);
};

export const updateUser = async (user:any, file:any = 'none') => {
  if (file !== 'none') {
    const logoRef = storageRef(storage, `userImages/${user.logoName}`);
    await deleteObject(logoRef);
    const {userLogoUrl, userLogoName} = await uploadLogo(file);
    user.logoUrl = userLogoUrl;
    user.logoName = userLogoName;
  }
  const reference = ref(db, `users/${user.username}`);
  await set(reference, user);
  return true;
};

export const userCheck = async (user:any) => {
  const phone = await get(query(ref(db, 'users'), orderByChild('phoneNumber'), equalTo(user.phone)));
  const email = await get(query(ref(db, 'users'), orderByChild('email'), equalTo(user.email)));
  const username = await get(ref(db, `users/${user.username}`));
  if (user.username.length < 2 || user.username.length > 20) {
    return 'Username must be between 2 and 20 characters';
  }
  if (username.val()) {
    return 'Username is already taken';
  }
  if (username.val() === 'undefined' || username.val() === 'null') {
    return 'Invalid username';
  }
  if (email.val()) {
    return 'A user with that email already exists';
  }
  if (phone.val()) {
    return 'A user with that phone already exists';
  }
};

export const deleteUser = async (user:any) => {

};

interface addonObj {
  [key: string]: string,
}
export const getUserAddons = async (username:any) => {
  const reference = ref(db, `users/${username}/addons`);
  const addons = await (await get(reference)).val();
  const obj = <addonObj>{};
  if (addons) {
    await Object.keys(addons).reduce(async (acc, addon:string) => {
      await acc;
      const res = await getAddon(addon);
      obj[addon] = res;
    }, Promise.resolve());
    return obj;
  } else {
    return null;
  }
};
export const blockUser = (userName:string, type:string) => {
  if (type === 'user') {
    type = 'blocked';
  } else {
    type = 'user';
  }
  set(ref(db, `users/${userName}/type`), type);
};

export const getUserByPhone = async (phone:string) => {
  const data = await get(query(ref(db, 'users'), orderByChild('phoneNumber'), equalTo(phone)));
  return data;
};
export const getUserByEmail = async (email:string) => {
  const data = await get(query(ref(db, 'users'), orderByChild('email'), equalTo(email)));
  return data;
};

