/* eslint-disable no-unused-vars */

import React, {useEffect, useState} from 'react';
// import AddonCard from '../../components/AddonCard';
import Main from '../../components/Main/Main';
import {getAllAddons} from '../../services/addon.service';
import AddonCard2 from '../../components/AddonCard2';
import {getAddonsByTags, getAddon} from '../../services/addon.service';
import {getUserByUsername} from '../../services/users.service';
// import {AppContext} from '../../providers/AppContext';
import {Link} from 'react-router-dom';
import Pagination from '../../components/Pagination';
const Home = () => {
  const [addons, setAddons] = useState<any[]>([]);
  const [featuredAddons, setFeaturedAddons] = useState<any[]>([]);
  const [popularAddons, setPopularAddons] = useState<any[]>([]);
  const [newestAddons, setNewestAddons] = useState<any[]>([]);
  const [searchAddons, setSearchAddons] = useState<any[]>([]);
  const [tag, setTag] = useState('');
  const [tags, setTags] = useState(['']);
  const [addonName, setAddonName] = useState('');
  const [userSearch, setUserSearch] = useState<any>();
  const [user, setUser] = useState('');
  const [sortBy, setSortBy] = useState('');
  const [sortResults, setSortResults] = useState<any[]>([]);
  const [explorin, setExplorin] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [addonsPerPage, setAddonsPerPage] = useState(10);
  const [allAddons, setAllAddons] = useState<any[]>([]);
  useEffect(() => {
    ( async () => {
      const data = await getAllAddons();
      const featuredData = await getAllAddons('featured');
      setFeaturedAddons(featuredData);
      if (data) {
        setAddons(data);
        setPopularAddons((Object.keys(data).sort((a, b) => (data[b].downloads - data[a].downloads ))).slice(0, 5));
        setNewestAddons(Object.keys(data).sort((a, b) => (new Date(data[b].uploadDate).getTime() - new Date(data[a].uploadDate).getTime())).slice(0, 5));
        setAllAddons({
          ...data,
          ...featuredData,
        });
      }
    })();
  }, []);
  const [prefix, setPrefix] = useState('Tags');
  const createTag = (e:any) => {
    e.preventDefault();
    const allTags = [...tags];
    if (allTags[0] === '') {
      allTags[0] = tag.trim();
    } else {
      if (allTags.includes(tag.trim())) {
        setTag('');
      } else {
        allTags.push(tag.trim());
      }
    }
    setTag('');
    setTags(allTags);
  };
  const updateTags = (e:any) => {
    e.target.value = e.target.value.replace(/[^\w\s]/gi, '');
    setTag(e.target.value);
  };
  const handlePrefix = (e:any) => {
    setPrefix(e.target.value);
  };
  const handleDeleteTag = (tag:string) => {
    const filteredTags = tags.filter((el) => el !== tag);
    setTags(filteredTags);
  };
  const updateUserName = (e:any) => {
    setUser(e.target.value);
  };
  const updateAddons = (e:any) => {
    setAddonName(e.target.value);
  };
  const searchDB = async () => {
    let data = null;
    if (prefix === 'Tags') {
      data = await getAddonsByTags(tags);
      setSearchAddons(data);
    } else if (prefix === 'Users') {
      data = await getUserByUsername(user);
      setUserSearch(data.val());
    } else if (prefix === 'Addons') {
      data = await getAddon(addonName);
      setSearchAddons(data);
    }
  };
  const handleSort = (e:any) => {
    if (e.target.value !== '') {
      setSortBy(e.target.value);
      let asorty = '';
      let sorted:any[] = [];
      if (e.target.value === 'name') {
        asorty = 'name';
        sorted = Object.keys(addons).sort((a:any, b:any) => (addons[a][asorty].localeCompare(addons[b][asorty])));
      } else if (e.target.value === 'creator') {
        asorty = 'creator';
        sorted = Object.keys(addons).sort((a:any, b:any) => (addons[a][asorty].localeCompare(addons[b][asorty])));
      } else if (e.target.value === 'downloads') {
        asorty = 'downloads';
        sorted = Object.keys(addons).sort((a:any, b:any) => (addons[b][asorty] - addons[a][asorty]));
      } else if (e.target.value === 'uploadDate') {
        asorty = 'uploadDate';
        sorted = Object.keys(addons).sort((a:any, b:any) => (new Date(addons[b][asorty]).getTime() - new Date(addons[a][asorty]).getTime()));
      } else if (e.target.value === 'commits') {
        asorty = 'lastCommit';
        sorted = Object.keys(addons).sort((a:any, b:any) => (new Date(addons[b][asorty]).getTime() - new Date(addons[a][asorty]).getTime()));
      }
      setSortResults(sorted);
    } else {
      setSortResults([]);
    }
  };
  const handleExploration = () => {
    setExplorin(!explorin);
  };
  const indexOfLastAddon = currentPage * addonsPerPage;
  const indexOfFirstAddon = indexOfLastAddon - addonsPerPage;
  const currentAddons = Object.fromEntries(
      Object.entries(allAddons).slice(indexOfFirstAddon, indexOfLastAddon),
  );
  const paginate = (pageNumber:any) => setCurrentPage(pageNumber);
  return (
    <Main title='Home'>
      <div className='my-4 mx-auto flex-wrap h-fit w-11/12  border-filler'>
        <div className='flex w-4/6 mx-auto'>
          <form onSubmit={(e) => createTag(e)} className='w-11/12' noValidate>
            <label className='block mx-20 my-4 text-textW text-sm font-bold mb-2' htmlFor='tag'>Searching <span className='text-sGreen'>{prefix}</span><span className='text-red-400'> *</span></label>
            <div className='ml-20'>
              {tags ? tags.map((tag, key) => tag !== '' ? <div key={key} id='addon-tag' onClick={() => handleDeleteTag(tag)} className='inline-block bg-filler border border-filler rounded-full px-3 py-1 text-sm font-semibold text-textW cursor-pointer mr-2 mb-2 hover:bg-sGreen hover:text-mainBG'>{tag}</div> : <></>) : <></>}
            </div>
            <div className='flex'>
              {/* <img src={searchIcon} className='w-8 h-8 my-auto mx-3'></img> */}
              <select onChange={handlePrefix} className='bg-mainBG text-textW w-20 m-0 p-0 text-center' defaultValue={'Tags'}>
                <option value='Tags'>Tags</option>
                <option value='Users'>Users</option>
                <option value='Addons'>Addons</option>
              </select>
              <input type='text' autoComplete='off' onChange={prefix === 'Tags' ? (e) => updateTags(e) : prefix === 'Users' ? (e) => updateUserName(e) : (e) => updateAddons(e)}
                value={prefix === 'Tags' ? tag : prefix === 'Users' ? user : addonName} className='my-2 shadow appearance-none border-filler border rounded w-full py-2 px-3 text-textW bg-navbar leading-tight focus:outline-none focus:shadow-outline' name='tag' id='tag' required maxLength={20} minLength={2}></input>
            </div>
          </form>
          <div className='w-1/12 self-end'>
            <button className='mx-2 justify-self-center border-2 border-sGreen my-2 text-sGreen font-bold bg-mainBG hover:bg-dGreen font-bold rounded-lg text-sm px-5 py-2.5 text-center dark:bg-mainBG dark:hover:bg-dGreen hover:text-mainBG dark:focus:ring-dGreen alg' onClick={searchDB}>Search</button>
          </div>
        </div>
        <div className=' text-textW w-4/6 mx-auto pb-2 h-14'>
          <span>Sort by:</span>
          <select onChange={handleSort} className='bg-mainBG text-sGreen text-center' defaultValue={'Name'}>
            <option value='name'>Name</option>
            <option value='creator'>Creator</option>
            <option value='downloads'>Downloads</option>
            <option value='uploadDate'>Upload Date</option>
            <option value='commits'>Last Commit</option>
          </select>
          {sortBy ? <button onClick={handleSort} value='' className='ml-4 text-sRed hover:cursor-pointer font-semibold'>
            <span className='flex w-8 h-8'>
              <svg xmlns="http://www.w3.org/2000/svg" viewBox='0 -10 24 24' className='fill-current'>
                <path d='M12.95 3.05a1 1 0 010 1.414L9.415 8l3.606 3.608a1 1 0 01.083 1.32l-.083.094a1 1 0 01-1.32.083l-.094-.083L8 9.414 4.465 12.95a1 1 0 01-1.414-1.414l3.535-3.537-3.464-3.463a1 1 0 01-.083-1.32l.083-.095a1 1 0 011.32-.083l.094.083L8 6.585l3.536-3.535a1 1 0 011.414 0z'></path>
              </svg>
            </span>
          </button> :<></>}
        </div>
        <h1 className='text-textW text-right text-sBlue hover:text-sGreen font-bold hover:cursor-pointer' onClick={handleExploration}>Explore All</h1>
        {!explorin ? <div>
          {searchAddons && searchAddons.length !== 0 ? <div>
            <h1 className='text-textW font-bold'>Search Results</h1>
            <div className='border border-filler'>
              <div className='mx-auto my-5 grid grid-cols-1 br1:grid-cols-3 br2:grid-cols-4 br3:grid-cols-5 br4:grid-cols-5 gap-2'>
                {searchAddons ? Object.keys(searchAddons).map((addonKey, key) => {
                  const keyTyped = addonKey as keyof typeof searchAddons;
                  const addon = searchAddons[keyTyped];
                  return (
                    <AddonCard2 key={key} {...addon}></AddonCard2>
                  );
                }) : <></>}
              </div>
            </div>
          </div> : <></> }
          {userSearch && userSearch.length !== 0 ? <div className=''>
            <Link to={`/user/${userSearch.username}`} state={{creator: userSearch.username}}>
              <div className='border border-filler h-80'>
                <div className='px-2'>
                  <img src={userSearch.logoUrl} className='object-fit mx-auto w-40 h-auto my-2'></img>
                </div>
                <div className='text-center'>
                  <h1 className={userSearch.type === 'blocked' ? 'font-semibold text-sRed' : userSearch.type === 'admin' ? 'font-semibold text-sBlue' : 'font-semibold text-sGreen'}><span className='text-sm'>Username: </span>{userSearch.username}</h1>
                  <h1>Email: {userSearch.email}</h1>
                  <h1>Phone: {userSearch.phoneNumber}</h1>
                </div>
              </div>
            </Link>
          </div> : <></>}
          {sortBy ? <div>
            <h1 className='text-textW font-bold'>Sort Results</h1>
            <div className='border border-filler'>
              <div className='mx-auto my-5 grid grid-cols-1 br1:grid-cols-3 br2:grid-cols-4 br3:grid-cols-5 br4:grid-cols-5 gap-2'>
                {sortResults ? sortResults.map((addonKey, key) => {
                  const keyTyped = addonKey as keyof typeof sortResults;
                  const addon = addons[keyTyped];
                  return (
                    <AddonCard2 key={key} {...addon}></AddonCard2>
                  );
                }) : <></>}
              </div>
            </div>
          </div> : <></>}
          <h1 className='text-textW font-bold'>Featured</h1>
          <div className='border border-filler'>
            <div className='mx-auto my-5 grid grid-cols-1 br1:grid-cols-3 br2:grid-cols-4 br3:grid-cols-5 br4:grid-cols-5 gap-2'>
              {featuredAddons ? Object.keys(featuredAddons).map((addonKey, key) => {
                const keyTyped = addonKey as keyof typeof featuredAddons;
                const addon = featuredAddons[keyTyped];
                return (
                  <AddonCard2 key={key} {...addon}></AddonCard2>
                );
              }) : <></>}
            </div>
          </div>
          <h1 className='text-textW font-bold'>Popular</h1>
          <div className='border border-filler'>
            <div className='mx-auto my-5 grid grid-cols-1 br1:grid-cols-3 br2:grid-cols-4 br3:grid-cols-5 br4:grid-cols-5 gap-2'>
              {popularAddons ? popularAddons.map((addonKey, key) => {
                const keyTyped = addonKey as keyof typeof addons;
                const addon = addons[keyTyped];
                return (
                  <AddonCard2 key={key} {...addon}></AddonCard2>
                );
              }) : <></>}
            </div>
          </div>
          <h1 className='text-textW font-bold'>New</h1>
          <div className='border border-filler'>
            <div className='mx-auto my-5 grid grid-cols-1 br1:grid-cols-2 br2:grid-cols-3 br3:grid-cols-5 br4:grid-cols-5 gap-2'>
              {newestAddons ? newestAddons.map((addonKey, key) => {
                const keyTyped = addonKey as keyof typeof addons;
                const addon = addons[keyTyped];
                return (
                  <AddonCard2 key={key} {...addon}></AddonCard2>
                );
              }) : <></>}
            </div>
          </div>
        </div> : <div>
          <h1 className='text-textW font-bold'>Exploring</h1>
          <div className=''>
            <div className='mx-auto my-5 grid grid-cols-1 br1:grid-cols-2 br2:grid-cols-3 br3:grid-cols-5 br4:grid-cols-5 gap-2'>
              {currentAddons ? Object.keys(currentAddons).map((addonKey, key) => {
                const keyTyped = addonKey as keyof typeof currentAddons;
                const addon = currentAddons[keyTyped];
                return (
                  <AddonCard2 key={key} {...addon}></AddonCard2>
                );
              }) : <></>}
            </div>
            <div className='flex border border-fillerD justify-center '>
              <Pagination addonsPerPage={addonsPerPage} totalAddons={Object.keys(addons).length} paginate={paginate} currentPage={currentPage}></Pagination>
            </div>
          </div>
        </div> }
      </div>
    </Main>
  );
};

export default Home;
//  addonLogo={addon.addonLogo} title={addon.name} description={addon.description} website={addon.originLink} price={addon.issues} rating={addon.rating} tags={addon.tags}
{/* <svg xmlns="http://www.w3.org/2000/svg" viewBox='0 0 24 24'>
<path d='M12.95 3.05a1 1 0 010 1.414L9.415 8l3.606 3.608a1 1 0 01.083 1.32l-.083.094a1 1 0 01-1.32.083l-.094-.083L8 9.414 4.465 12.95a1 1 0 01-1.414-1.414l3.535-3.537-3.464-3.463a1 1 0 01-.083-1.32l.083-.095a1 1 0 011.32-.083l.094.083L8 6.585l3.536-3.535a1 1 0 011.414 0z'></path>
</svg> */}
