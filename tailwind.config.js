module.exports = {
  content: [
    './src/**/*.{js,jsx,ts,tsx}',
  ],
  theme: {
    extend: {
      colors: {
        navbar: '#14181e',
        textW: '#f8f8f8',
        mainBG: '#06070d',
        textG: '#30313a',
        sGreen: '#00e59b',
        sBlue: '#7579ff',
        sRed: '#D70022',
        fillerD: '#242a2f',
        filler: '#5d5d6c',
        black: '#161a17',
        dGreen: '#00d38f',
        dPurple: '#570861',
      },
      screens: {
        br1: '725px',
        br2: '1055px',
        br3: '1385px',
        br4: '1715px',
      },
    },
  },
  plugins: [],
};
